import pickle
import os
from typing import Callable, Any
from enum import Enum
from loguru import logger


class RepositoryTypes(Enum):
    WISHING_WELL_ENTRIES = "wishing_well"
    DONATIONS = "donations"
    DISCORD_MESSAGES = "discord_messages"
    DISCORD_CHANNELS = "discord_channels"


class DataPersistenceManager:
    def __init__(self, data_path: str):
        if not os.path.exists(data_path):
            os.mkdir(data_path)
        self.data_path = data_path
        self.registered: dict[str, Callable[[], Any]] = dict()

    def register_data(self, default_factory: Callable[[], Any], name: str) -> None:
        self.registered[name] = default_factory

    def save_data(self, data: Any, name: str) -> None:
        with open(f"{self.data_path}/{name}.pkl", "wb") as file:
            pickle.dump(data, file)

    def _load_data(self, name: str) -> Any | None:
        try:
            if os.path.exists(self.data_path):
                with open(f"{self.data_path}/{name}.pkl", "rb") as file:
                    return pickle.load(file)
            else:
                return None
        except Exception as e:
            print(f"Failed to load data: {e}")
            return None

    def load_or_init_data(self, name: str) -> Any:
        if name not in self.registered:
            raise ValueError("Register the repository first before loading")

        default_factory = self.registered[name]
        data = self._load_data(name=name)
        if data is None:
            logger.info(f"{name} file not found; creating and saving")
            data = default_factory()
            self.save_data(data=data, name=name)
        else:
            logger.info(
                f"Saved {name} found; initializing repository from the save file"
            )
        return data
