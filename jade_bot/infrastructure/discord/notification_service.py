from discord.channel import TextChannel
from discord.threads import Thread
from infrastructure.discord.discord_service import DiscordService
from domain.models.system_dto import DiscordChannelConfig


class DiscordNotificationService:
    def __init__(
        self, discord_service: DiscordService, channel_config: DiscordChannelConfig
    ):
        self.discord_service = discord_service
        self.channel_config = channel_config

    async def notify(self, message: str) -> None:
        channel = await self.discord_service.get_channel(self.channel_config)
        if channel and (
            isinstance(channel, TextChannel) or isinstance(channel, Thread)
        ):
            await channel.send(message)
