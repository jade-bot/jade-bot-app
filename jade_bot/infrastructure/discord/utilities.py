from discord.ext import commands
from discord.member import Member
import discord
from functools import wraps
from enum import Enum
from typing import Any, Callable


class Role(Enum):
    COMMANDER = "Commanders"
    OFFICER = "Officers"
    SENIOR_OFFICER = "Senior Officers"
    LEADER = "Leaders"


def check_user_roles(roles_allowed: tuple[Role, ...]) -> Any:
    def decorator(func: Callable) -> Any:
        @wraps(func)
        async def wrapper(*args: Any, **kwargs: Any) -> Any:
            interaction: discord.Interaction = args[1]
            allowed_role_names = [role.value for role in roles_allowed]

            if not interaction.guild:
                await interaction.response.send_message(
                    "check_user_roles can only be used within the server"
                )
                return

            guild_roles = await interaction.guild.fetch_roles()
            if not guild_roles:
                await interaction.response.send_message("No roles found in the server")
                return

            allowed_role_ids = {
                role.id for role in guild_roles if role.name in allowed_role_names
            }

            if isinstance(interaction.user, Member):
                user: Member = interaction.user
                user_role_ids = set(role.id for role in user.roles)
                if not user_role_ids.intersection(allowed_role_ids):
                    await interaction.response.send_message(
                        f"Only {', '.join(allowed_role_names)} can use this command!"
                    )
                else:
                    return await func(*args, **kwargs)
            else:
                await interaction.response.send_message(
                    "This command can only be used as a server member"
                )
                return

        return wrapper

    return decorator
