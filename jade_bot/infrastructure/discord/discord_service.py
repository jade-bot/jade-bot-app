import discord
from discord.ext.commands.bot import Bot
from discord.utils import get
from discord.errors import NotFound

from infrastructure.discord.utilities import Role

from domain.models.system_dto import DiscordChannelConfig, DiscordMessageConfig
from domain.exceptions.discord_exceptions import (
    ChannelNotFoundException,
    MessageNotFoundException,
    CannotCreateThreadException,
    RoleNotFoundException,
    MessageNotAvailableException,
)
from loguru import logger


class DiscordService:
    def __init__(self, bot: Bot):
        self.bot = bot

    async def get_message(
        self,
        channel_config: DiscordChannelConfig,
        message_config: DiscordMessageConfig,
    ) -> discord.Message:
        channel = await self.get_channel(channel_config)
        try:
            message = await channel.fetch_message(message_config.message_id)
        except NotFound:
            raise MessageNotFoundException(message=message_config)

        if not message:
            raise MessageNotFoundException(message=message_config)
        elif message.author.id != self.bot.user.id:  # type:ignore
            raise MessageNotAvailableException(message=message)

        return message

    async def get_channel(
        self, channel_config: DiscordChannelConfig
    ) -> discord.guild.TextChannel | discord.threads.Thread:
        channel = self.bot.get_channel(channel_config.channel_id)
        if (
            not isinstance(channel, discord.guild.TextChannel)
            and not isinstance(channel, discord.threads.Thread)
        ) or not channel:
            raise ChannelNotFoundException(channel_config)

        return channel

    async def update_stored_message(
        self,
        channel_config: DiscordChannelConfig,
        message_config: DiscordMessageConfig,
        message_content: str,
    ) -> None:
        message = await self.get_message(
            channel_config=channel_config, message_config=message_config
        )

        await message.edit(content=message_content)

    async def send_message(
        self, channel_config: DiscordChannelConfig, message_content: str
    ) -> discord.Message:
        channel = await self.get_channel(channel_config)
        message = await channel.send(message_content)
        return message

    async def delete_stored_message(
        self, channel_config: DiscordChannelConfig, message_config: DiscordMessageConfig
    ) -> None:
        channel = await self.get_channel(channel_config)
        try:
            message = await channel.fetch_message(message_config.message_id)

            if message:
                await message.delete()
        except discord.NotFound:
            return

    async def rename_channel(
        self, channel_config: DiscordChannelConfig, new_name: str
    ) -> None:
        channel = await self.get_channel(channel_config)
        await channel.edit(name=new_name)

    async def create_thread(
        self,
        channel_config: DiscordChannelConfig,
        thread_name: str,
        message: discord.Message,
    ) -> discord.Thread:
        channel = await self.get_channel(channel_config)
        if isinstance(channel, discord.Thread):
            raise CannotCreateThreadException(channel_config)

        thread = await channel.create_thread(name=thread_name, message=message)

        return thread

    async def get_role_mention(self, role: Role) -> str:
        role_instance = get(self.bot.guilds[0].roles, name=role.value)
        if not role_instance:
            raise RoleNotFoundException(role=role.value)
        return role_instance.mention

    @staticmethod
    async def send_message_to_thread(
        thread: discord.Thread, message_content: str
    ) -> discord.Message:
        message = await thread.send(message_content)
        return message

    @staticmethod
    async def add_reaction(message: discord.Message, reactions: list) -> None:
        for reaction in reactions:
            await message.add_reaction(reaction)
