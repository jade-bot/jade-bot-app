from infrastructure.gw2api.models.game.guild.log import (
    GuildLogEntry,
    GuildLogEntryStash,
)
from infrastructure.data_processing.base_processor import BaseDataProcessor
from domain.models.donations_dto import BankEntry, ItemDonation, GoldDonation
from infrastructure.gw2api.api import GW2API
from typing import Any
from enum import Enum

import pandas as pd

pd.options.mode.chained_assignment = None


class EntryOperation(Enum):
    DONATION = "donation"
    WITHDRAWAL = "withdrawal"


def _api_dto_to_dict(entry: GuildLogEntryStash) -> dict[str, Any]:
    return {
        "member": entry.user,
        "gold": entry.coins,
        "item_id": entry.item_id,
        "amount": entry.count,
        "operation": entry.operation,
        "date": entry.time,
    }


def _get_member_item_donations(
    entries: pd.DataFrame, member: str
) -> list[ItemDonation]:
    member_entries = entries[entries.member == member]
    return [
        ItemDonation(
            item_id=entry.item_id,
            name=entry.item_name,
            price=entry.price,
            amount=entry.amount,
        )
        for i, entry in member_entries.iterrows()
    ]


class DonationsProcessor(BaseDataProcessor):
    MC_ID = 19976

    def __init__(self, api: GW2API) -> None:
        super().__init__(allowed_types=(GuildLogEntryStash,))
        self.api = api

    def _filter_entries(self, entries: list[GuildLogEntry]) -> list[GuildLogEntryStash]:
        entries_filtered = [
            entry for entry in entries if type(entry) in self._allowed_types
        ]

        filters = (lambda x: x.item_id != DonationsProcessor.MC_ID,)

        entries_filtered = self.apply_filters(entries_filtered, filters)
        return entries_filtered

    async def _get_price_per_item(self, item_id: int) -> int:
        price = await self.api.commerce.get_item_price(item_id)
        unit_price: int = price.sells.unit_price
        return unit_price

    async def _get_name_per_item(self, item_id: int) -> str:
        item = await self.api.items.get_item(item_id)
        if item:
            item_name: str = item.name
            return item_name
        else:
            return f"Unknown item (id {item_id})"

    async def _fetch_prices(self, item_ids: list[int]) -> dict[int, int]:
        return {item: await self._get_price_per_item(item) for item in item_ids}

    async def _fetch_names(self, item_ids: list[int]) -> dict[int, str]:
        return {item: await self._get_name_per_item(item) for item in item_ids}

    async def _process_gold_entries(
        self, entries: list[GuildLogEntryStash], operation: EntryOperation
    ) -> tuple[set[str], dict[str, GoldDonation]]:
        donators = set()
        entries_gold = [_api_dto_to_dict(entry) for entry in entries if entry.coins > 0]

        if entries_gold:
            entries_grouped_gold = (
                self._convert_operation_to_sign(pd.DataFrame(entries_gold), "gold")
                .groupby(by=["member"], as_index=False)
                .sum()
            )
            filter_criteria = (
                lambda x: x > 0 if operation == EntryOperation.DONATION else x < 0
            )
            entries_grouped_filtered_gold: pd.DataFrame = entries_grouped_gold.loc[
                entries_grouped_gold["gold"].apply(filter_criteria)
            ]

            donators.update(list(entries_grouped_filtered_gold.member.unique()))

            dto_gold_entries = {
                entry.member: GoldDonation(gold=entry.gold)
                for i, entry in entries_grouped_filtered_gold.iterrows()
            }
        else:
            dto_gold_entries = {}

        return donators, dto_gold_entries

    async def _process_item_entries(
        self, entries: list[GuildLogEntryStash], operation: EntryOperation
    ) -> tuple[set[str], dict[str, ItemDonation]]:
        donators = set()

        entries_items = [
            _api_dto_to_dict(entry) for entry in entries if entry.count > 0
        ]
        if entries_items:
            entries_grouped_items = (
                self._convert_operation_to_sign(pd.DataFrame(entries_items), "amount")
                .groupby(by=["member", "item_id"], as_index=False)
                .sum()
            )

            filter_criteria = (
                lambda x: x > 0 if operation == EntryOperation.DONATION else x < 0
            )
            entries_grouped_filtered_items: pd.DataFrame = entries_grouped_items.loc[
                entries_grouped_items["amount"].apply(filter_criteria)
            ]

            donators.update(list(entries_grouped_filtered_items.member.unique()))

            items_donated = [
                int(i) for i in entries_grouped_filtered_items.item_id.unique()
            ]

            item_prices = await self._fetch_prices(items_donated)
            item_names = await self._fetch_names(items_donated)

            entries_grouped_filtered_items["price"] = entries_grouped_filtered_items[
                "item_id"
            ].apply(lambda x: item_prices[x])
            entries_grouped_filtered_items[
                "item_name"
            ] = entries_grouped_filtered_items["item_id"].apply(lambda x: item_names[x])

            dto_item_entries = {
                member: _get_member_item_donations(
                    entries_grouped_filtered_items, member
                )
                for member in donators
            }
        else:
            dto_item_entries = {}

        return donators, dto_item_entries

    async def preprocess_log(
        self, entries: list[GuildLogEntry]
    ) -> tuple[list[GuildLogEntryStash], int, bool]:
        non_null_entries = [entry for entry in entries if entry]
        null_entries_found = len(non_null_entries) != len(entries)

        non_null_entries.sort(key=lambda x: x.time)
        last_log_id = non_null_entries[-1].id

        entries_filtered: list[GuildLogEntryStash] = self._filter_entries(entries)

        return entries_filtered, last_log_id, null_entries_found

    async def process_entries(
        self, entries: list[GuildLogEntryStash], operation: EntryOperation
    ) -> list[BankEntry] | None:
        gold_members, gold_entries = await self._process_gold_entries(
            entries=entries, operation=operation
        )
        item_members, item_entries = await self._process_item_entries(
            entries=entries, operation=operation
        )

        dto_entries = [
            BankEntry(
                member=member,
                gold=gold_entries.get(member),
                items=item_entries.get(member),
            )
            for member in list(gold_members | item_members)
        ]

        return dto_entries
