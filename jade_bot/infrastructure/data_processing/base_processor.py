from typing import Callable, Any

import pandas as pd


class BaseDataProcessor:
    def __init__(self, allowed_types: tuple = ()):
        self._allowed_types = allowed_types

    @staticmethod
    def apply_filters(
        entries: list[Any],
        filters: tuple[Callable[[Any], Any]],
        aggregation_method: Callable[[Any], bool] = all,
    ) -> list[Any]:
        combined_filter: Callable[[Any], bool] = lambda x: aggregation_method(
            f(x) for f in filters
        )

        return list(filter(combined_filter, entries))

    @staticmethod
    def _convert_operation_to_sign(df: pd.DataFrame, column: str) -> pd.DataFrame:
        if "operation" not in df.columns:
            raise ValueError(
                f'operation not found in the record data. Existing columns: {", ".join(df.columns)}'
            )
        df[column] = df.apply(
            lambda x: x[column] if x["operation"] == "deposit" else x[column] * (-1),
            axis=1,
        )
        df = df.drop(columns="operation")
        return df
