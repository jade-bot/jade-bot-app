from infrastructure.gw2api.models.game.guild.log import (
    GuildLogEntry,
    GuildLogEntryStash,
)
from infrastructure.data_processing.base_processor import BaseDataProcessor
from domain.models.wishing_well_dto import WishingWellEntry
import pandas as pd


class WishingWellProcessor(BaseDataProcessor):
    MC_ID = 19976
    BASE_DONATION = 2

    def __init__(self) -> None:
        super().__init__(allowed_types=(GuildLogEntryStash,))

    def _filter_entries(self, entries: list[GuildLogEntry]) -> list[GuildLogEntryStash]:
        entries_filtered = [
            entry for entry in entries if type(entry) in self._allowed_types
        ]

        filters = (lambda x: x.item_id == WishingWellProcessor.MC_ID,)

        entries_filtered = self.apply_filters(entries_filtered, filters)
        return entries_filtered

    def process_entries(
        self, entries: list[GuildLogEntry]
    ) -> tuple[list[WishingWellEntry] | None, int, bool]:
        non_null_entries = [entry for entry in entries if entry]
        null_entries_found = len(non_null_entries) != len(entries)

        non_null_entries.sort(key=lambda x: x.time)
        last_log_id = non_null_entries[-1].id

        entries_filtered: list[GuildLogEntryStash] = self._filter_entries(entries)

        if not entries_filtered:
            return [], last_log_id, null_entries_found

        entries_list = [
            {
                "member": entry.user,
                "mc_amount": entry.count,
                "operation": entry.operation,
                "date": entry.time,
                "entry_log_id": entry.id,
            }
            for entry in entries_filtered
        ]

        entries_grouped = (
            self._convert_operation_to_sign(pd.DataFrame(entries_list), "mc_amount")
            .groupby(by=["member"], as_index=False)
            .sum()
        )

        entries_grouped_filtered: pd.DataFrame = entries_grouped[
            entries_grouped["mc_amount"] > 0
        ]

        dto_entries = [
            WishingWellEntry(**entry)
            for i, entry in entries_grouped_filtered.iterrows()
        ]

        return dto_entries, last_log_id, null_entries_found
