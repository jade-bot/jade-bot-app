from dataclasses import dataclass


@dataclass
class PriceOffer:
    quantity: int
    unit_price: float


@dataclass
class Price:
    id: int
    whitelisted: bool
    buys: PriceOffer | dict[str, ...]
    sells: PriceOffer | dict[str, ...]

    def __post_init__(self) -> None:
        self.buys = (
            PriceOffer(**self.buys) if isinstance(self.buys, dict) else self.buys
        )
        self.sells = (
            PriceOffer(**self.sells) if isinstance(self.sells, dict) else self.sells
        )
