from dataclasses import dataclass, field


@dataclass
class InfixUpgradeAttribute:
    attribute: str
    modifier: int


@dataclass
class InfixUpgradeBuff:
    skill_id: int
    description: str | None = field(default=None)


@dataclass
class InfixUpgrade:
    id: int
    attributes: list[InfixUpgradeAttribute | dict[str, ...]] = field(
        default_factory=list
    )
    buff: InfixUpgradeBuff | dict[str, ...] | None = field(default=None)

    def __post_init__(self) -> None:
        self.attributes = [
            InfixUpgradeAttribute(**elem) if isinstance(elem, dict) else elem
            for elem in self.attributes
        ]
        if self.buff and isinstance(self.buff, dict):
            self.buff = InfixUpgradeBuff(**self.buff)


@dataclass
class InfusionSlot:
    flags: list[str] = field(default_factory=list)
    item_id: int | None = field(default=None)
