from dataclasses import dataclass, field
from .item_details import (
    ItemArmorDetails,
    ItemBackDetails,
    ItemBagDetails,
    ItemConsumableDetails,
    ItemContainerDetails,
    ItemGatheringToolDetails,
    ItemGizmoDetails,
    ItemMiniatureDetails,
    ItemSalvageKitDetails,
    ItemTrinketDetails,
    ItemUpgradeComponentDetails,
    ItemWeaponDetails,
)


@dataclass
class ItemUpgrade:
    upgrade: str
    item_id: int


@dataclass
class Item:
    id: int
    chat_link: str = field(repr=False)
    name: str
    icon: str | None = field(repr=False)
    type: str = field(repr=False)
    rarity: str
    level: int
    vendor_value: int = field(repr=False)
    flags: list[str] = field(repr=False)
    game_types: list[str] = field(repr=False)
    restrictions: list[str] = field(repr=False)
    description: str | None = field(repr=False, default=None)
    default_skin: int | None = field(default=None, repr=False)
    upgrades_into: list[ItemUpgrade | dict[str, ...]] | None = field(
        default=None, repr=False
    )
    upgrades_from: list[ItemUpgrade | dict[str, ...]] | None = field(
        default=None, repr=False
    )
    # details: ItemArmorDetails | \
    #          ItemBackDetails | \
    #          ItemBagDetails | \
    #          ItemConsumableDetails | \
    #          ItemContainerDetails | \
    #          ItemGatheringToolDetails | \
    #          ItemGizmoDetails | \
    #          ItemMiniatureDetails | \
    #          ItemSalvageKitDetails | \
    #          ItemTrinketDetails | \
    #          ItemUpgradeComponentDetails | \
    #          ItemWeaponDetails | dict[str, ...] | None \
    #     = field(default=None, repr=False, init=False)


@dataclass
class ItemArmor(Item):
    details: ItemArmorDetails | dict[str, ...] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.details = (
            ItemArmorDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemBack(Item):
    details: ItemBackDetails | dict[str, ...] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.details = (
            ItemBackDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemBag(Item):
    details: ItemBagDetails | dict[str, ...] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.details = (
            ItemBagDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemConsumable(Item):
    details: ItemConsumableDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemConsumableDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemContainer(Item):
    details: ItemContainerDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemContainerDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemGatheringTool(Item):
    details: ItemGatheringToolDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemGatheringToolDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemGizmo(Item):
    details: ItemGizmoDetails | dict[str, ...] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.details = (
            ItemGizmoDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemMiniature(Item):
    details: ItemMiniatureDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemMiniatureDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemSalvageKit(Item):
    details: ItemSalvageKitDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemSalvageKitDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemTrinket(Item):
    details: ItemTrinketDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemTrinketDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemUpgradeComponent(Item):
    details: ItemUpgradeComponentDetails | dict[str, ...] | None = field(
        default=None, repr=False
    )

    def __post_init__(self) -> None:
        self.details = (
            ItemUpgradeComponentDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )


@dataclass
class ItemWeapon(Item):
    details: ItemWeaponDetails | dict[str, ...] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.details = (
            ItemWeaponDetails(**self.details)
            if isinstance(self.details, dict)
            else self.details
        )
