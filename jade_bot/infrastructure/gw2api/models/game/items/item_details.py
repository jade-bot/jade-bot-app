from dataclasses import dataclass, field
from .item_subobject import InfixUpgrade, InfusionSlot


@dataclass
class _ItemWearableDetails:
    attribute_adjustment: int = field(repr=False)
    secondary_suffix_item_id: str = field(repr=False)
    infix_upgrade: InfixUpgrade | dict[str, ...] | None = field(
        default=None, repr=False
    )
    suffix_item_id: int | None = field(default=None, repr=False)
    stat_choices: list[int] | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        if self.infix_upgrade and isinstance(self.infix_upgrade, dict):
            self.infix_upgrade = InfixUpgrade(**self.infix_upgrade)


@dataclass
class ItemArmorDetails(_ItemWearableDetails):
    type: str = field(default="")
    weight_class: str = field(default="")
    defense: int = field(default=0)
    infusion_slots: list[InfusionSlot | dict[str, ...]] = field(
        default_factory=list, repr=False
    )

    def __post_init__(self) -> None:
        super().__post_init__()
        self.infusion_slots = [
            InfusionSlot(**elem) if isinstance(elem, dict) else elem
            for elem in self.infusion_slots
        ]


@dataclass
class ItemBackDetails(_ItemWearableDetails):
    pass


@dataclass
class ItemBagDetails:
    size: int
    no_sell_or_sort: bool


@dataclass
class ItemConsumableDetails:
    type: str
    description: str | None = field(default=None)
    duration_ms: str | None = field(default=None)
    unlock_type: str | None = field(default=None)
    color_id: str | None = field(default=None, repr=False)
    recipe_id: str | None = field(default=None, repr=False)
    extra_recipe_ids: str | None = field(default=None, repr=False)
    guild_upgrade_id: str | None = field(default=None, repr=False)
    apply_count: str | None = field(default=None, repr=False)
    name: str | None = field(default=None)
    icon: str | None = field(default=None, repr=False)
    skins: str | None = field(default=None, repr=False)


@dataclass
class ItemContainerDetails:
    type: str


@dataclass
class ItemGatheringToolDetails:
    type: str


@dataclass
class ItemGizmoDetails:
    type: str
    guild_upgrade_id: int | None = field(default=None, repr=False)
    vendor_ids: list[int] | None = field(default=None, repr=False)


@dataclass
class ItemMiniatureDetails:
    minipet_id: int = field(repr=False)


@dataclass
class ItemSalvageKitDetails:
    type: str
    charges: int


@dataclass
class ItemTrinketDetails(ItemBackDetails):
    type: str = field(default="")


@dataclass
class ItemUpgradeComponentDetails:
    type: str
    suffix: str
    flags: list[str] = field(default_factory=list, repr=False)
    infusion_upgrade_flags: list[str] = field(default_factory=list, repr=False)
    infix_upgrade: InfixUpgrade | dict[str, ...] | None = field(
        default=None, repr=False
    )
    attribute_adjustment: int = field(default=0)

    def __post_init__(self) -> None:
        if self.infix_upgrade and isinstance(self.infix_upgrade, dict):
            self.infix_upgrade = InfixUpgrade(**self.infix_upgrade)


@dataclass
class ItemWeaponDetails(_ItemWearableDetails):
    type: str = field(default="")
    damage_type: str = field(default="")
    min_power: int = field(default=0)
    max_power: int = field(default=0)
    defense: int = field(default=0, repr=False)
    infusion_slots: list[InfusionSlot | dict[str, ...]] | None = field(
        default_factory=list, repr=False
    )
