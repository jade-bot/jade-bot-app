from dataclasses import dataclass, field


@dataclass
class GuildLogEntry:
    id: int
    time: str
    type: str
    user: str | None = field(default=None)


@dataclass
class GuildLogEntryInvite(GuildLogEntry):
    invited_by: str = field(default="")


@dataclass
class GuildLogEntryInviteDeclined(GuildLogEntry):
    declined_by: str = field(default="")


@dataclass
class GuildLogEntryKick(GuildLogEntry):
    kicked_by: str = field(default="")


@dataclass
class GuildLogEntryRankChange(GuildLogEntry):
    changed_by: str = field(default="")
    old_rank: str = field(default="")
    new_rank: str = field(default="")


@dataclass
class GuildLogEntryTreasury(GuildLogEntry):
    item_id: int = field(default=0)
    count: int = field(default=0)


@dataclass
class GuildLogEntryStash(GuildLogEntry):
    operation: str = field(default="")
    item_id: int = field(default=0)
    count: int = field(default=0)
    coins: int = field(default=0)


@dataclass
class GuildLogEntryMotD(GuildLogEntry):
    motd: str = field(default="")


@dataclass
class GuildLogEntryUpgrade(GuildLogEntry):
    item_id: int = field(default=0)
    count: int = field(default=0)
    action: str = field(default="")
    upgrade_id: int = field(default=0)
    recipe_id: int | None = field(default=None)


@dataclass
class GuildLogEntryInfluence(GuildLogEntry):
    activity: str = field(default="")
    total_participants: int = field(default=0)
    participants: list[str] = field(default_factory=list)


@dataclass
class GuildLogEntryMission(GuildLogEntry):
    state: str = field(default="")
    influence: int = field(default=0)
