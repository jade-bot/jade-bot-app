from dataclasses import dataclass, field


@dataclass
class GuildMember:
    name: str
    rank: str
    joined: str = field(repr=False)
