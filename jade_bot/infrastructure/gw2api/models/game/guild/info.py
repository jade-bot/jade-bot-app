from dataclasses import dataclass, field


@dataclass
class GuildEmblemElement:
    id: int
    colors: list[int]


@dataclass
class GuildEmblem:
    background: GuildEmblemElement | dict[str, ...]
    foreground: GuildEmblemElement | dict[str, ...]
    flags: list[str] = field(default_factory=list)

    def __post_init__(self) -> None:
        self.background = (
            GuildEmblemElement(**self.background)
            if isinstance(self.background, dict)
            else self.background
        )
        self.foreground = (
            GuildEmblemElement(**self.foreground)
            if isinstance(self.foreground, dict)
            else self.foreground
        )


@dataclass
class GuildInfo:
    id: int
    name: str
    tag: str
    emblem: GuildEmblem | dict[str, ...] = field(repr=False)
    level: int | None = field(default=None)
    motd: str | None = field(default=None, repr=False)
    influence: int | None = field(default=None, repr=False)
    aetherium: str | None = field(default=None, repr=False)
    resonance: int | None = field(default=None, repr=False)
    favor: int | None = field(default=None, repr=False)
    member_count: int | None = field(default=None)
    member_capacity: int | None = field(default=None, repr=False)

    def __post_init__(self) -> None:
        self.emblem = (
            GuildEmblem(**self.emblem) if isinstance(self.emblem, dict) else self.emblem
        )
