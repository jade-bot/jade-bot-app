from infrastructure.gw2api.models.game.items.item import (
    ItemArmor,
    ItemBack,
    ItemBag,
    ItemConsumable,
    ItemContainer,
    ItemGatheringTool,
    ItemGizmo,
    ItemMiniature,
    ItemSalvageKit,
    ItemTrinket,
    ItemUpgradeComponent,
    ItemWeapon,
    Item,
)
from infrastructure.gw2api.models.game.commerce import Price
from infrastructure.gw2api.models.game.guild.log import (
    GuildLogEntryInvite,
    GuildLogEntryInviteDeclined,
    GuildLogEntryKick,
    GuildLogEntryMotD,
    GuildLogEntryRankChange,
    GuildLogEntryStash,
    GuildLogEntryUpgrade,
    GuildLogEntryTreasury,
    GuildLogEntry,
    GuildLogEntryInfluence,
    GuildLogEntryMission,
)
from infrastructure.gw2api.models.game.guild.members import GuildMember
from infrastructure.gw2api.models.game.guild.info import GuildInfo
import json
from typing import Any, Callable

#
# def ensure_dict_input(fn: Callable[..., ...]) -> Any:  # TODO check if actually needed
#     def inner(*args: Any) -> Any:
#         assert len(args) == 2
#         data: str = args[1]
#         data = json.loads(data) if isinstance(data, str) else data
#         return fn(args[0], data)
#
#     return inner


class _BaseFactory:
    def __init__(self, response_class: type[Any]):
        self._ResponseClass = response_class

    # @ensure_dict_input
    def get_elem(self, data: dict[str, ...]) -> Any:
        return self._ResponseClass(**data)

    # @ensure_dict_input
    def get_batch(self, data: list[dict[str, ...]]) -> list[...]:
        return [self.get_elem(elem) for elem in data] if data else []


class _TypedFactory:
    def __init__(self, types: dict[str, ...]):
        self._types = types

    # @ensure_dict_input
    def get_elem(self, data: dict[str, ...]) -> Any:
        try:
            return self._types[data["type"]](**data)
        except Exception as e:
            print(e.__traceback__)
            print(data)

    # @ensure_dict_input
    def get_batch(self, data: list[dict[str, ...]]) -> list[...]:
        return [self.get_elem(elem) for elem in data]


class PriceFactory(_BaseFactory):
    def __init__(self) -> None:
        super().__init__(response_class=Price)


class ItemFactory(_TypedFactory):
    TYPES = {
        "Armor": ItemArmor,
        "Back": ItemBack,
        "Bag": ItemBag,
        "Consumable": ItemConsumable,
        "Container": ItemContainer,
        "CraftingMaterial": Item,
        "Gathering": ItemGatheringTool,
        "Gizmo": ItemGizmo,
        "JadeTechModule": Item,
        "Key": Item,
        "MiniPet": ItemMiniature,
        "PowerCore": Item,
        "Tool": ItemSalvageKit,
        "Trait": Item,
        "Trinket": ItemTrinket,
        "Trophy": Item,
        "UpgradeComponent": ItemUpgradeComponent,
        "Weapon": ItemWeapon,
    }

    def __init__(self) -> None:
        super().__init__(types=ItemFactory.TYPES)


class GuildLogFactory(_TypedFactory):
    TYPES = {
        "joined": GuildLogEntry,
        "invited": GuildLogEntryInvite,
        "invite_declined": GuildLogEntryInviteDeclined,
        "kick": GuildLogEntryKick,
        "rank_change": GuildLogEntryRankChange,
        "treasury": GuildLogEntryTreasury,
        "stash": GuildLogEntryStash,
        "motd": GuildLogEntryMotD,
        "upgrade": GuildLogEntryUpgrade,
        "influence": GuildLogEntryInfluence,
        "mission": GuildLogEntryMission,
    }

    def __init__(self) -> None:
        super().__init__(types=GuildLogFactory.TYPES)


class GuildMemberFactory(_BaseFactory):
    def __init__(self) -> None:
        super().__init__(response_class=GuildMember)


class GuildInfoFactory(_BaseFactory):
    def __init__(self) -> None:
        super().__init__(response_class=GuildInfo)
