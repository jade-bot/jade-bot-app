from infrastructure.gw2api.interface.items import ItemsAPI
from infrastructure.gw2api.interface.commerce import CommerceAPI
from infrastructure.gw2api.interface.guild import GuildAPI


class GW2API:
    def __init__(self, guild_id: str):
        self.items = ItemsAPI()
        self.commerce = CommerceAPI()
        self.guild = GuildAPI(guild_id)
