from ._endpoint import ApiEndpoint
from infrastructure.gw2api.models.factories import ItemFactory
from infrastructure.gw2api.models.game.items.item import Item
from typing import Any


class ItemsAPI:
    def __init__(self) -> None:
        self._endpoint = ApiEndpoint()
        self._endpoint.set_name("items")
        self._response_factory = ItemFactory()

    async def get_item(self, item_id: str | int) -> Item:
        metadata, response = await self._endpoint.get_one(params={"ids": item_id})
        item: Item = self._response_factory.get_elem(response)
        return item

    async def get_items(self, item_ids: list[str | int]) -> list[Item]:
        metadata, response = await self._endpoint.get_list(
            params={"ids": [",".join(str(_id) for _id in item_ids)]}
        )
        items: list[Item] = self._response_factory.get_batch(response)
        return items

    async def get_all_items(self) -> dict[str, Any] | list[str] | None:
        metadata, response = await self._endpoint.get_list(params={})
        return response  # method returns plain ids
