from ..interface import BASE_URL
from typing import Any
import logging
import os
import nest_asyncio

# import requests
import aiohttp
from infrastructure.gw2api.exceptions import (
    GW2APIReturnedNotOkException,
    GW2APIFailureParsingException,
    GW2APIEndpointNotConfiguredException,
)

nest_asyncio.apply()

logger = logging.getLogger(__name__)


class ApiEndpoint:
    name: str

    def __init__(self):
        self.session = aiohttp.ClientSession()

    def set_name(self, name: str) -> None:
        self.name = name

    async def _get(
        self, path: str, **params: Any
    ) -> tuple[dict[str, ...], dict[str, ...] | None]:
        if self.name:
            if path != "":
                path = f"/{path}"
            request_query = f"{BASE_URL}/{self.name}{path}"

            path_cleaned = request_query.split("?")[0]
            async with self.session.get(
                request_query, params=params, ssl=False
            ) as response_raw:
                if not response_raw.ok:
                    raise GW2APIReturnedNotOkException(
                        method=path_cleaned,
                        status=response_raw.status,
                        reason=response_raw.reason,
                    )
                try:
                    response = await response_raw.json()
                except Exception as e:
                    logging.info(
                        f"Request couldn't be processed: {e}", self.name, path, **params
                    )
                    raise GW2APIFailureParsingException(method=path_cleaned)

                return self._get_metadata(response_raw), response
        else:
            raise GW2APIEndpointNotConfiguredException(
                endpoint_class=type(self).__name__
            )

    @staticmethod
    def _get_metadata(r: aiohttp.ClientResponse) -> dict[str, ...]:
        metadata = {}

        for key, link in r.links.items():
            metadata[key] = link["url"]

        if "x-page-total" in r.headers:
            metadata["page_total"] = int(r.headers["x-page-total"])
        if "x-page-size" in r.headers:
            metadata["page_size"] = int(r.headers["x-page-size"])
        if "x-result-total" in r.headers:
            metadata["result_total"] = int(r.headers["x-result-total"])
        if "x-result-count" in r.headers:
            metadata["result_count"] = int(r.headers["x-result-count"])

        return metadata

    async def get_one(
        self, path: str | None = None, params: dict[str, ...] | None = None
    ) -> tuple[dict[str, ...], dict[str, ...] | None]:
        if not path:
            path = ""
        if not params:
            params = dict()

        metadata, response = await self._get(path, **params)
        if isinstance(response, list):
            response = response[0]
        return metadata, response

    async def get_list(
        self, path: str | None = None, params: dict[str, ...] | None = None
    ) -> tuple[dict[str, ...], dict[str, ...] | list[str] | None]:
        if not path:
            path = ""
        if not params:
            params = dict()

        return await self._get(path, **params)

    async def get_page(
        self, path: str | None = None, params: dict[str, ...] | None = None
    ) -> None:
        pass  # TODO


class AuthenticatedApiEndpoint(ApiEndpoint):
    def __init__(self) -> None:
        super().__init__()
        self._token = os.environ["AUTHENTICATION_TOKEN"]

    async def _get(
        self, path: str, **params: Any
    ) -> tuple[dict[str, ...], dict[str, ...] | None]:
        # headers = params.setdefault("headers", {})
        # headers.setdefault("Authorization", f"Bearer {self._token}")
        path = f"{path}?access_token={self._token}"

        return await super()._get(path, **params)
