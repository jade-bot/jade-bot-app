from ._endpoint import AuthenticatedApiEndpoint
from infrastructure.gw2api.models.factories import (
    GuildLogFactory,
    GuildMemberFactory,
    GuildInfoFactory,
)
from infrastructure.gw2api.models.game.guild.log import GuildLogEntry
from infrastructure.gw2api.models.game.guild.members import GuildMember
from infrastructure.gw2api.models.game.guild.info import GuildInfo


class GuildAPI:
    def __init__(self, guild_id: str):
        self._endpoint = AuthenticatedApiEndpoint()
        self._endpoint.set_name("guild")
        self.guild_id = guild_id

    async def get_log(self, since: str | None = None) -> list[GuildLogEntry]:
        metadata, response = await self._endpoint.get_list(
            path=f"{self.guild_id}/log", params={"since": since} if since else {}
        )
        response_factory = GuildLogFactory()
        if response is not None:
            entries: list[GuildLogEntry] = response_factory.get_batch(response)
            return entries
        else:
            return []

    async def get_members(self) -> list[GuildMember]:
        metadata, response = await self._endpoint.get_list(
            path=f"{self.guild_id}/members"
        )
        response_factory = GuildMemberFactory()
        members: list[GuildMember] = response_factory.get_batch(response)
        return members

    # def get_ranks(self):
    #     metadata, response = self._endpoint.get_list(path=f'{self.guild_id}/ranks')
    #     return self._response_factory.get_batch(response)
    # #
    # def get_stash(self):
    #     metadata, response = self._endpoint.get_list(path=f'stash/{self.guild_id}')
    #     return self._response_factory.get_batch(response)
    #
    # def get_storage(self):
    #     metadata, response = self._endpoint.get_list(path=f'storage/{self.guild_id}')
    #     return self._response_factory.get_batch(response)
    #
    # def get_treasury(self):
    #     metadata, response = self._endpoint.get_list(path=f'treasury/{self.guild_id}')
    #     return self._response_factory.get_batch(response)
    #
    async def get_info(self) -> GuildInfo:
        metadata, response = await self._endpoint.get_one(path=f"{self.guild_id}")
        response_factory = GuildInfoFactory()
        info: GuildInfo = response_factory.get_elem(response)
        return info
