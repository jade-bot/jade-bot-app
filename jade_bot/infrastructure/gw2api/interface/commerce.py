from ._endpoint import ApiEndpoint
from infrastructure.gw2api.models.game.commerce import Price
from infrastructure.gw2api.models.factories import PriceFactory


class CommerceAPI:
    def __init__(self) -> None:
        self._endpoint = ApiEndpoint()
        self._endpoint.set_name("commerce")
        self._response_factory = PriceFactory()

    async def get_item_price(self, item_id: int | str) -> Price:
        metadata, response = await self._endpoint.get_one(
            path="prices", params={"ids": item_id}
        )
        price: Price = self._response_factory.get_elem(response)
        return price

    async def get_items_prices(self, item_ids: list[int | str]) -> list[Price]:
        metadata, response = await self._endpoint.get_list(
            path="prices", params={"ids": [",".join(str(_id) for _id in item_ids)]}
        )
        prices: list[Price] = self._response_factory.get_batch(response)
        return prices
