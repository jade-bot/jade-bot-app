class GW2APIException(Exception):
    def __init__(self, msg: str | None = None):
        if msg:
            self.msg = msg
        else:
            self.msg = ""

    def __repr__(self) -> str:
        return self.msg

    def __str__(self) -> str:
        return self.msg


class GW2APIReturnedNotOkException(GW2APIException):
    def __init__(self, method, status, reason):
        super().__init__(msg=f"{method} returned non OK status: {status} - {reason}")


class GW2APIFailureParsingException(GW2APIException):
    def __init__(self, method):
        super().__init__(msg=f"Failure parsing response from {method}")


class GW2APIEndpointNotConfiguredException(GW2APIException):
    def __init__(self, endpoint_class):
        super().__init__(
            msg=f"Please set an endpoint name first using set_name method: {endpoint_class}"
        )
