from .templates.scheduled_messages_templates import GM_VOTING, EVENTS_REMINDER
from datetime import date, timedelta


class ScheduledMessagesFormatter:
    @staticmethod
    def _get_next_sunday_date() -> str:
        today = date.today()
        sunday = today + timedelta((6 - today.weekday()) % 7)
        return sunday.strftime("%d %B")

    @staticmethod
    def format_gm_voting(officers_tag: str) -> str:
        return GM_VOTING.substitute(
            officers_tag=officers_tag,
            next_gm_date=ScheduledMessagesFormatter._get_next_sunday_date(),
        )

    @staticmethod
    def format_events_reminder(commanders_tag: str) -> str:
        return EVENTS_REMINDER.substitute(commanders_tag=commanders_tag)
