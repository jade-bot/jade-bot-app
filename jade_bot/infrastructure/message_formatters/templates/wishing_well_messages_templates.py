from string import Template

PUBLIC_ENTRIES_LIST = Template(
    """
# TOTAL MYSTIC COINS [$mc_total]
# :four_leaf_clover: PARTICIPANTS [$members_total] :four_leaf_clover:
$member_entries

Last updated: <t:$last_update_ts:R>
"""
)

PRIVATE_ENTRIES_LIST = Template(
    """
**Anonymous:** 
    $anonymous_entries MCs
        
**Members:**
$member_entries
"""
)
