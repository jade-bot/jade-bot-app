from string import Template

CONFIG_LIST = Template(
    """
**Channel configs**:
$channel_configs_list

**Message configs**:
$message_configs_list
"""
)
