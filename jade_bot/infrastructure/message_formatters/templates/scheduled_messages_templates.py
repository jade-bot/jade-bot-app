from string import Template


GM_VOTING = Template(
    """
$officers_tag
[GMs $next_gm_date] 

:one: I want to lead! 
:two:  I can lead if needed 
:three: I want to lead but i'm a bit unsure/ don't know all the gms 
:four: I will join missions 
:five: I'm not sure if i can join missions 
:six: I won't join missions
"""
)

EVENTS_REMINDER = Template(
    """
$commanders_tag last few hours to add your events for next week (deadline is the start of Guild Missions)
# https://bit.ly/jade-event-calendar
Please double check your events, so no mistakes are made!
"""
)
