from domain.models.wishing_well_dto import WishingWellEntry
from .templates.wishing_well_messages_templates import (
    PUBLIC_ENTRIES_LIST,
    PRIVATE_ENTRIES_LIST,
)


class WishingWellMessagesFormatter:
    @staticmethod
    def _format_list(entries: list[WishingWellEntry], show_amount: bool) -> str:
        return (
            "\n".join([e.format(show_amount=show_amount) for e in entries])
            if entries
            else "No entries yet"
        )

    @staticmethod
    def _get_sum(anonymous_entries: int, member_entries: list[WishingWellEntry]) -> int:
        return int(sum(e.mc_amount for e in member_entries) + anonymous_entries)

    @staticmethod
    def format_public_list(
        anonymous_entries: int,
        member_entries: list[WishingWellEntry],
        last_update_ts: int,
    ) -> str:
        return PUBLIC_ENTRIES_LIST.substitute(
            mc_total=WishingWellMessagesFormatter._get_sum(
                anonymous_entries, member_entries
            ),
            members_total=len(member_entries),
            member_entries=WishingWellMessagesFormatter._format_list(
                member_entries, show_amount=False
            ),
            last_update_ts=last_update_ts,
        )

    @staticmethod
    def format_private_list(
        anonymous_entries: int, member_entries: list[WishingWellEntry]
    ) -> str:
        return PRIVATE_ENTRIES_LIST.substitute(
            anonymous_entries=anonymous_entries,
            member_entries=WishingWellMessagesFormatter._format_list(
                member_entries, show_amount=True
            ),
        )
