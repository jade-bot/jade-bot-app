from domain.models.system_dto import StoredChannelType, StoredMessageType
from .templates.system_messages_templates import CONFIG_LIST


class SystemMessagesFormatter:
    @staticmethod
    def _format_dict(configs: dict[StoredChannelType | StoredMessageType, str]) -> str:
        return "* " + "\n* ".join(
            [f"{_type.value}: {link}" for _type, link in configs.items()]
        )

    @staticmethod
    def format_config_list(
        channel_configs: dict[StoredChannelType, str],
        message_configs: dict[StoredMessageType, str],
    ) -> str:
        return CONFIG_LIST.substitute(
            channel_configs_list=SystemMessagesFormatter._format_dict(channel_configs),
            message_configs_list=SystemMessagesFormatter._format_dict(message_configs),
        )
