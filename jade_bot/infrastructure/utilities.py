from functools import wraps
import traceback
from domain.exceptions.base_exception import JadeBotException
from infrastructure.gw2api.exceptions import GW2APIException
from typing import Any, Callable


def handle_command_errors(func: Callable) -> Any:
    @wraps(func)
    async def wrapper(*args: Any, **kwargs: Any) -> Any:
        try:
            return await func(*args, **kwargs)
        except JadeBotException as e:
            return str(e)
        except GW2APIException as e:
            return str(e)
        except Exception as e:
            return f"Something went horribly wrong on my end, please call Tess to fix me: \n```{traceback.format_exc()}```"

    return wrapper


def format_gold(gold: int) -> str:
    gold_convert = {"g": 100 * 100, "s": 100, "c": 1}
    res = {}
    for coin, v in gold_convert.items():
        res[coin], gold = divmod(gold, v)

    return " ".join([f"{value}{key}" for key, value in res.items()])
