import nest_asyncio

import discord
from discord.ext import commands

from loguru import logger

nest_asyncio.apply()


def setup_intents() -> discord.Intents:
    intents = discord.Intents.default()
    intents.message_content = True
    intents.emojis = True
    intents.guild_messages = True
    intents.guild_reactions = True
    intents.webhooks = True
    intents.members = True

    return intents


def get_bot() -> commands.Bot:
    client = commands.Bot(command_prefix="/", intents=setup_intents())
    client.remove_command("help")

    @client.event
    async def on_ready() -> None:
        logger.info(f"{client.user} has connected to Discord!")
        await client.tree.sync()
        logger.info("Synced")

    return client
