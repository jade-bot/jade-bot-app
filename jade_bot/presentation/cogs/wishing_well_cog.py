from discord.ext import commands
from discord import app_commands
import discord
from loguru import logger
from application.commands.wishing_well_commands import WishingWellCommands
from infrastructure.discord.utilities import check_user_roles, Role


class WishingWellCog(commands.Cog):
    """
    Group of commands `/wishingwell [...]` to manage monthly Wishing Well
    """

    wishing_well = app_commands.Group(
        name="wishingwell", description="Manage wishing well"
    )

    def __init__(
        self, bot: commands.Bot, commands_service: WishingWellCommands
    ) -> None:
        self.bot = bot
        self._commands_service = commands_service

    @wishing_well.command(name="new", description="Make a WW for the new month")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def make_new(self, interaction: discord.Interaction, month: str) -> None:
        logger.info(f"ww/make_new called by {interaction.user}")
        response = await self._commands_service.make_new(month=month)
        await interaction.response.send_message(response)

    @wishing_well.command(
        name="update", description="Manually update WW entries from the guild bank"
    )
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def update(self, interaction: discord.Interaction) -> None:
        logger.info(f"ww/update called by {interaction.user}")
        await interaction.response.defer()
        response = await self._commands_service.update()
        await interaction.followup.send(response)

    @wishing_well.command(name="add-entry", description="Manually add new WW entry")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def add_entry(
        self, interaction: discord.Interaction, member_id: str, mc_amount: int
    ) -> None:
        await interaction.response.defer()
        logger.info(
            f"ww/add_entry called by {interaction.user} with params: {member_id}, {mc_amount}"
        )
        response = await self._commands_service.add_entry(member_id, mc_amount)
        await interaction.followup.send(response)

    @wishing_well.command(
        name="withdraw-member-all", description="Withdraw all member's MCs from the WW"
    )
    @check_user_roles(roles_allowed=(Role.SENIOR_OFFICER, Role.LEADER))
    async def withdraw_member(
        self, interaction: discord.Interaction, member_id: str
    ) -> None:
        logger.info(f"ww/withdraw_member called by {interaction.user}")
        response = await self._commands_service.withdraw_member(member_id)
        await interaction.response.send_message(response)

    @wishing_well.command(
        name="withdraw-member-excess",
        description="Withdraw member's excess MCs from the WW",
    )
    @check_user_roles(roles_allowed=(Role.SENIOR_OFFICER, Role.LEADER))
    async def withdraw_member_excess(
        self, interaction: discord.Interaction, member_id: str
    ) -> None:
        logger.info(f"ww/withdraw_member_excess called by {interaction.user}")
        response = await self._commands_service.withdraw_member_excess(member_id)
        await interaction.response.send_message(response)

    @wishing_well.command(name="add-anonymous-entry", description="Add anonymous entry")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def add_anonymous_entry(
        self, interaction: discord.Interaction, mc_amount: int
    ) -> None:
        logger.info(f"ww/add_anonymous_entry called by {interaction.user}")
        response = await self._commands_service.add_anonymous_entry(mc_amount)
        await interaction.response.send_message(response)

    @wishing_well.command(
        name="withdraw-anonymous-entry", description="Withdraw MC from anonymous entry"
    )
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def withdraw_anonymous_entry(
        self, interaction: discord.Interaction, mc_amount: int
    ) -> None:
        logger.info(f"ww/withdraw_anonymous_entry called by {interaction.user}")
        response = await self._commands_service.withdraw_anonymous_entry(mc_amount)
        await interaction.response.send_message(response)

    @wishing_well.command(
        name="get-all-entries", description="List all entries with MCs"
    )
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def get_all_entries(self, interaction: discord.Interaction) -> None:
        logger.info(f"ww/get_all_entries called by {interaction.user}")
        response = await self._commands_service.get_all_entries()
        await interaction.response.send_message(response)
