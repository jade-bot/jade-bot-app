from discord import Embed, Color, app_commands
from discord.ext import commands
import discord


class HelpCommandCog(commands.Cog):
    """
    Custom help provider
    """

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.PREFIX = "/"
        self.VERSION = "v. 2.0.4"
        self.DEFAULT_OWNER = "tesserata"

    @app_commands.command(name="help", description="Get interactive commands info")
    @app_commands.choices(
        module=[
            app_commands.Choice(name="/system", value="SystemCog"),
            app_commands.Choice(name="/wishingwell", value="WishingWellCog"),
            app_commands.Choice(name="/donations", value="DonationsCog"),
        ]
    )
    async def get_help(
        self,
        interaction: discord.Interaction,
        module: str | None = None,
    ) -> None:
        if module is None:
            await interaction.response.send_message(
                embed=await self.create_overview_embed(interaction)
            )
        else:
            await interaction.response.send_message(
                embed=await self.create_module_embed(module)
            )

    async def create_overview_embed(self, interaction: discord.Interaction) -> Embed:
        owner_mention = self.get_owner_mention(interaction)
        embed = Embed(
            title="Commands and modules",
            color=Color.blue(),
            description=f"Use `{self.PREFIX}help <module>` to get more information about that module",
        )

        embed.add_field(name="Modules", value=self.get_cogs_description(), inline=False)
        embed.add_field(
            name="Not belonging to a module",
            value=self.get_commands_description(),
            inline=False,
        )
        embed.add_field(
            name="About",
            value=f"Jade bot is created and maintained by {owner_mention}\n"
            "Please, let me know about any errors and difficulties you encounter using it.",
            inline=False,
        )
        embed.set_footer(text=f"Bot is running {self.VERSION}")
        return embed

    def get_owner_mention(self, interaction: discord.Interaction) -> str:
        owner_id = 230712796046819329
        if interaction.guild:
            owner = interaction.guild.get_member(owner_id)
            return owner.mention if owner else self.DEFAULT_OWNER
        return self.DEFAULT_OWNER

    def get_cogs_description(self) -> str:
        cogs_desc = ""
        for cog_name, cog in self.bot.cogs.items():
            if cog_name != "BackgroundCog":
                cogs_desc += f"**{cog_name}**:\t{cog.__doc__}\n"
        return cogs_desc

    def get_commands_description(self) -> str:
        commands_desc = ""
        for command in self.bot.commands:
            if not command.cog_name and not command.hidden:
                commands_desc += f"{command.name} - {command.description}\n"
        return commands_desc if commands_desc else "None"

    async def create_module_embed(self, module_name: str) -> Embed:
        for cog_name, cog in self.bot.cogs.items():
            if cog_name.lower() == module_name.lower():
                embed = Embed(
                    title=f"{cog_name} - Commands",
                    description=cog.__doc__,
                    color=Color.green(),
                )
                await self.add_commands_to_embed(embed, cog)
                return embed
        return Embed(title="Error", description="Module not found.", color=Color.red())

    async def add_commands_to_embed(self, embed: Embed, cog: commands.Cog) -> None:
        group_name = ""
        for command in cog.walk_app_commands():
            if isinstance(command, app_commands.Group):
                group_name = command.name
            else:
                command_params = [
                    f"{p.name}*" if p.required else p.name for p in command.parameters
                ]
                embed.add_field(
                    name=f"`{self.PREFIX}{group_name} {command.name} {' '.join(command_params)}`",
                    value=command.description,
                    inline=False,
                )
