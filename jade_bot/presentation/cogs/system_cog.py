import discord
from discord.ext import commands
from discord import app_commands
from loguru import logger

from application.commands.system_commands import SystemCommands
from infrastructure.discord.utilities import check_user_roles, Role
from domain.models.system_dto import StoredChannelType, StoredMessageType


class SystemCog(commands.Cog):
    """
    Group of commands `/system [...]` to manage the bot configuration and system tasks
    """

    system = app_commands.Group(name="system", description="Manage bot config")

    def __init__(self, bot: commands.Bot, commands_service: SystemCommands) -> None:
        self.bot = bot
        self._commands_service = commands_service

    @system.command(name="save-data", description="Save bot state")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def save_data(self, interaction: discord.Interaction) -> None:
        logger.info(f"sys/save_data called by {interaction.user}")
        await interaction.response.defer()
        response = await self._commands_service.save_app_data()
        await interaction.followup.send(response)

    @system.command(name="set-channel", description="Manually set channel ID")
    @check_user_roles(roles_allowed=(Role.SENIOR_OFFICER, Role.LEADER))
    @app_commands.choices(
        channel_type=[
            app_commands.Choice(
                name="Wishing well channel",
                value=StoredChannelType.WISHING_WELL_CHANNEL.value,
            ),
            app_commands.Choice(
                name="Bank channel", value=StoredChannelType.BANK_CHANNEL.value
            ),
            app_commands.Choice(
                name="Commanders channel",
                value=StoredChannelType.COMMANDERS_CHANNEL.value,
            ),
            app_commands.Choice(
                name="Voting channel", value=StoredChannelType.VOTING_CHANNEL.value
            ),
            app_commands.Choice(
                name="Bot log channel", value=StoredChannelType.BOT_LOG_CHANNEL.value
            ),
        ]
    )
    async def set_channel(
        self,
        interaction: discord.Interaction,
        channel_type: app_commands.Choice[str],
        channel_id: str,
    ) -> None:
        response = await self._commands_service.set_channel(
            channel_type=channel_type.value, channel_id=int(channel_id)
        )
        await interaction.response.send_message(response)

    @system.command(name="set-message", description="Manually set message ID")
    @check_user_roles(roles_allowed=(Role.SENIOR_OFFICER, Role.LEADER))
    @app_commands.choices(
        message_type=[
            app_commands.Choice(
                name="Wishing well list",
                value=StoredMessageType.WW_MESSAGE.value,
            ),
        ]
    )
    async def set_message(
        self,
        interaction: discord.Interaction,
        message_type: app_commands.Choice[str],
        message_id: str,
    ) -> None:
        response = await self._commands_service.set_message(
            message_type=message_type.value, message_id=int(message_id)
        )
        await interaction.response.send_message(response)

    @system.command(name="get-config", description="Get current configuration")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def get_config(self, interaction: discord.Interaction) -> None:
        response = await self._commands_service.get_config()
        await interaction.response.send_message(response)

    @system.command(name="last-log-id", description="Show last saved log id")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    @app_commands.choices(
        log_type=[
            app_commands.Choice(
                name="Wishing well log",
                value="wishing well",
            ),
            app_commands.Choice(name="Donations log", value="donations"),
        ]
    )
    async def get_last_log_id(
        self,
        interaction: discord.Interaction,
        log_type: app_commands.Choice[str],
    ):
        id = None
        if log_type.value == "wishing well":
            id = await self._commands_service.get_last_ww_id()
        elif log_type.value == "donations":
            id = await self._commands_service.get_last_ww_id()
        await interaction.response.send_message(f"Last {log_type.name} id: {id}")
