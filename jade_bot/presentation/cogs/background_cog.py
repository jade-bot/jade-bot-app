from discord.ext import commands, tasks
from application.commands.wishing_well_commands import WishingWellCommands
from application.commands.donations_commands import DonationsCommands
from application.commands.background_commands import BackgroundCommands
from application.commands.system_commands import SystemCommands
from datetime import time, datetime
from loguru import logger
import asyncio


class BackgroundCog(commands.Cog):
    def __init__(
        self,
        bot: commands.Bot,
        ww_commands_service: WishingWellCommands,
        donations_commands_service: DonationsCommands,
        background_commands_service: BackgroundCommands,
        system_commands_service: SystemCommands,
    ) -> None:
        self.bot = bot
        self._ww_commands_service = ww_commands_service
        self._donations_command_service = donations_commands_service
        self._background_commands_service = background_commands_service
        self._system_commands_service = system_commands_service

    @commands.Cog.listener()
    async def on_ready(self) -> None:
        await asyncio.sleep(30)
        if not self.background_ww_update.is_running():
            logger.info("Starting background WW update routine")
            self.background_ww_update.start()
        if not self.background_donations_update.is_running():
            logger.info("Starting background donations update routine")
            self.background_donations_update.start()
        if not self.background_save_data.is_running():
            logger.info("Starting background backup routine")
            self.background_save_data.start()
        if not self.post_gm_vote.is_running():
            logger.info("Starting background GM vote routine")
            self.post_gm_vote.start()
        if not self.post_events_reminder.is_running():
            logger.info("Starting background commanders reminder routine")
            self.post_events_reminder.start()

    @tasks.loop(minutes=30)
    async def background_save_data(self) -> None:
        await self._system_commands_service.save_app_data()

    @tasks.loop(minutes=30)
    async def background_ww_update(self) -> None:
        await self._ww_commands_service.update()

    @tasks.loop(minutes=30)
    async def background_donations_update(self) -> None:
        await asyncio.sleep(5 * 60)
        await self._donations_command_service.update()

    @tasks.loop(time=time(14, 0, 0, 0))
    async def post_gm_vote(self) -> None:
        if datetime.today().weekday() == 3:
            logger.info("I'm trying to post a gm vote")
            await self._background_commands_service.publish_gm_vote()

    @tasks.loop(time=time(14, 0, 0, 0))
    async def post_events_reminder(self) -> None:
        if datetime.today().weekday() == 6:
            logger.info("I'm trying to post an events reminder")
            await self._background_commands_service.publish_event_reminder()
