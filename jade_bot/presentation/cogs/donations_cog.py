import discord
from discord.ext import commands
from discord import app_commands
from loguru import logger

from application.commands.donations_commands import DonationsCommands
from infrastructure.discord.utilities import check_user_roles, Role


class DonationsCog(commands.Cog):
    """
    Group of commands `/donations [...]` to manage the bot configuration and system tasks
    """

    donations = app_commands.Group(name="donations", description="Manage bot config")

    def __init__(self, bot: commands.Bot, commands_service: DonationsCommands) -> None:
        self.bot = bot
        self._commands_service = commands_service

    @donations.command(name="update", description="Manually check donations")
    @check_user_roles(roles_allowed=(Role.OFFICER, Role.SENIOR_OFFICER, Role.LEADER))
    async def update_donations(self, interaction: discord.Interaction) -> None:
        logger.info(f"donations/update called by {interaction.user}")
        await interaction.response.defer()
        response = await self._commands_service.update()
        await interaction.followup.send(response)
