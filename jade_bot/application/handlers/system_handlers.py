from application.handlers.base_handler import BaseEventHandler
from domain.events.domain_events.system_events import (
    ChannelConfigChangedEvent,
    MessageConfigChangedEvent,
)
from infrastructure.discord.discord_service import DiscordService
from infrastructure.discord.notification_service import (
    DiscordNotificationService,
)


class ChannelConfigChangedEventHandler(BaseEventHandler):
    def __init__(
        self,
        notification_service: DiscordNotificationService,
        discord_service: DiscordService,
    ):
        super().__init__(notification_service=notification_service)
        self.discord_service = discord_service

    async def handle(self, event: ChannelConfigChangedEvent) -> None:
        channel = await self.discord_service.get_channel(event.channel_config)
        message = f"""{event.channel_config.channel_type.value} has been set to {channel.jump_url}"""
        await self.notification_service.notify(message)


class MessageConfigChangedEventHandler(BaseEventHandler):
    def __init__(
        self,
        notification_service: DiscordNotificationService,
        discord_service: DiscordService,
    ):
        super().__init__(notification_service=notification_service)
        self.discord_service = discord_service

    async def handle(self, event: MessageConfigChangedEvent) -> None:
        message = await self.discord_service.get_message(
            channel_config=event.channel_config,
            message_config=event.message_config,
        )
        await self.notification_service.notify(
            f"""{event.message_config.message_type.value} has been set to {message.jump_url}"""
        )
