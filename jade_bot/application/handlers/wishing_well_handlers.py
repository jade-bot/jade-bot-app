from application.handlers.base_handler import BaseEventHandler
from domain.events.domain_events.wishing_well_events import (
    WWUpdateFetchedEvent,
    RepeatedEntryEvent,
    ExcessMCDepositedEvent,
    MemberLeftEvent,
)
from infrastructure.utilities import format_gold


class WWUpdateFetchedEventHandler(BaseEventHandler):
    async def handle(self, event: WWUpdateFetchedEvent) -> None:
        entries_list = "\n* ".join([e.format() for e in event.entries])
        message = f"""Wishing well updated: {len(event.entries)} new entries found:\n* {entries_list}"""
        await self.notification_service.notify(message)


class RepeatedEntryEventHandler(BaseEventHandler):
    async def handle(self, event: RepeatedEntryEvent) -> None:
        worth_formatted = format_gold(event.entry.mc_amount * event.mc_worth)
        message = f":warning: Repeated entry added :warning: \n**{event.entry.member}** contributed another {event.entry.mc_amount} MCs worth {worth_formatted}\n"
        await self.notification_service.notify(message)


class ExcessMCDepositedEventHandler(BaseEventHandler):
    async def handle(self, event: ExcessMCDepositedEvent) -> None:
        worth_formatted = format_gold(event.excess_amount * event.mc_worth)
        message = f":warning: Excess entry added :warning:\n**{event.entry.member}** contributed {event.excess_amount} extra MCs worth {worth_formatted}\n"
        await self.notification_service.notify(message)


class MemberLeftEventHandler(BaseEventHandler):
    async def handle(self, event: MemberLeftEvent) -> None:
        message = f":warning: Member left the guild and was withdrawn from WW: {event.member} :warning:"
        await self.notification_service.notify(message)
