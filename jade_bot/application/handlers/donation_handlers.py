from application.handlers.base_handler import BaseEventHandler
from infrastructure.utilities import format_gold
from domain.events.domain_events.donation_events import (
    BankUpdateFetchedEvent,
    DonationFoundEvent,
    WithdrawalFoundEvent,
)


class BankUpdateFetchedEventHandler(BaseEventHandler):
    async def handle(self, event: BankUpdateFetchedEvent) -> None:
        members_list = "\n* ".join(event.members)
        message = f"""Bank entries updated: {event.amount} new entries found from:\n* {members_list}"""
        await self.notification_service.notify(message)


class DonationFoundEventHandler(BaseEventHandler):
    async def handle(self, event: DonationFoundEvent) -> None:
        entry = event.entry
        message = (
            f"**Donation from {entry.member}:**\nTotal {format_gold(entry.total_worth)}"
        )
        if entry.gold.value > 0:
            message += f"\n\n**{entry.gold.format()} raw**"
        if entry.items:
            message += (
                f"\n\n**Items donated:**\n{entry.format_shorten_item_donations()}"
            )

        await self.notification_service.notify(message)


class WithdrawalFoundEventHandler(BaseEventHandler):
    async def handle(self, event: WithdrawalFoundEvent) -> None:
        entry = event.entry
        message = f"**Withdrawal from {entry.member}:**\nTotal {format_gold(entry.total_worth)}"
        if entry.gold.value < 0:
            message += f"\n\n**{entry.gold.format()} raw**"
        if entry.items:
            message += (
                f"\n\n**Items withdrawn:**\n{entry.format_shorten_item_donations()}"
            )

        await self.notification_service.notify(message)
