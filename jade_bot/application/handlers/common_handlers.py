from application.handlers.base_handler import BaseEventHandler
from domain.events.domain_events.common_events import (
    FaultyApiEvent,
)


class FaultyApiEventHandler(BaseEventHandler):
    async def handle(self, event: FaultyApiEvent) -> None:
        message = f":warning: Encountered an API problem: {event.fault_type} :warning:"
        await self.notification_service.notify(message)
