from abc import ABC, abstractmethod
from domain.events.domain_events.base_event import BaseEvent
from infrastructure.discord.notification_service import (
    DiscordNotificationService,
)


class BaseEventHandler(ABC):
    def __init__(self, notification_service: DiscordNotificationService):
        self.notification_service = notification_service

    @abstractmethod
    async def handle(self, event: BaseEvent) -> None:
        pass
