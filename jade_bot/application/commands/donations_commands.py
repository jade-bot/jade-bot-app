from domain.services.donation_service import DonationsService

from infrastructure.utilities import handle_command_errors
from infrastructure.discord.discord_service import DiscordService
from domain.config_processing_service import ConfigProcessingService


class DonationsCommands:
    def __init__(
        self,
        service: DonationsService,
        discord_service: DiscordService,
        config_processor: ConfigProcessingService,
    ):
        self._service = service
        self._discord_service = discord_service
        self._config_processor = config_processor

    @handle_command_errors
    async def update(self) -> str:
        new_entries = await self._service.update()
        return f"Successfully updated! {len(new_entries)} new donations found."
