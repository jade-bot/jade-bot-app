from infrastructure.utilities import handle_command_errors
from infrastructure.discord.discord_service import DiscordService
from infrastructure.discord.utilities import Role
from infrastructure.message_formatters.scheduled_messages_formatter import (
    ScheduledMessagesFormatter,
)
from domain.config_processing_service import ConfigProcessingService
from domain.models.system_dto import StoredChannelType


class BackgroundCommands:
    def __init__(
        self,
        discord_service: DiscordService,
        config_processor: ConfigProcessingService,
    ):
        self._message_formatter = ScheduledMessagesFormatter
        self._discord_service = discord_service
        self._config_processor = config_processor

    @handle_command_errors
    async def publish_event_reminder(self) -> None:
        commanders_tag = await self._discord_service.get_role_mention(
            role=Role.COMMANDER
        )
        message_content = self._message_formatter.format_events_reminder(
            commanders_tag=commanders_tag
        )
        channel = self._config_processor.get_channel_config(
            channel_type=StoredChannelType.COMMANDERS_CHANNEL
        )
        await self._discord_service.send_message(
            channel_config=channel, message_content=message_content
        )

    @handle_command_errors
    async def publish_gm_vote(self) -> None:
        officers_tag = await self._discord_service.get_role_mention(role=Role.OFFICER)
        message_content = self._message_formatter.format_gm_voting(
            officers_tag=officers_tag
        )
        channel = self._config_processor.get_channel_config(
            channel_type=StoredChannelType.VOTING_CHANNEL
        )
        message = await self._discord_service.send_message(
            channel_config=channel, message_content=message_content
        )
        await self._discord_service.add_reaction(
            message=message,
            reactions=[
                str(emoji) for emoji in ("1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣")
            ],
        )
