from domain.services.wishing_well_service import WishingWellService
from domain.models.system_dto import StoredMessageType, StoredChannelType

from infrastructure.message_formatters.wishing_well_messages_formatter import (
    WishingWellMessagesFormatter,
)
from infrastructure.utilities import handle_command_errors
from infrastructure.discord.discord_service import DiscordService
from domain.config_processing_service import ConfigProcessingService


class WishingWellCommands:
    def __init__(
        self,
        service: WishingWellService,
        discord_service: DiscordService,
        config_processor: ConfigProcessingService,
    ):
        self._service = service
        self._message_formatter = WishingWellMessagesFormatter
        self._discord_service = discord_service
        self._config_processor = config_processor

    @handle_command_errors
    async def _make_public_message(self) -> str:
        anonymous_total, members_total = await self._service.get_all_entries()
        last_update_ts = await self._service.get_last_update()

        public_message: str = WishingWellMessagesFormatter.format_public_list(
            anonymous_entries=anonymous_total,
            member_entries=members_total,
            last_update_ts=last_update_ts,
        )

        return public_message

    @handle_command_errors
    async def _update_stored_message(self) -> None:
        public_message = await self._make_public_message()

        await self._discord_service.update_stored_message(
            channel_config=self._config_processor.get_channel_config(
                channel_type=StoredChannelType.WISHING_WELL_CHANNEL
            ),
            message_config=self._config_processor.get_message_config(
                message_type=StoredMessageType.WW_MESSAGE
            ),
            message_content=public_message,
        )

    @handle_command_errors
    async def make_new(self, month: str) -> str:
        await self._service.clear()

        channel_config = self._config_processor.get_channel_config(
            channel_type=StoredChannelType.WISHING_WELL_CHANNEL
        )
        await self._discord_service.rename_channel(
            channel_config=channel_config, new_name=f"{month}-wishing-well"
        )

        message_config = self._config_processor.get_message_config(
            message_type=StoredMessageType.WW_MESSAGE
        )

        if message_config.message_id:
            await self._discord_service.delete_stored_message(
                channel_config=channel_config, message_config=message_config
            )

        new_message = await self._make_public_message()
        message = await self._discord_service.send_message(
            channel_config=channel_config, message_content=new_message
        )

        self._config_processor.set_message_config(
            channel_type=StoredChannelType.WISHING_WELL_CHANNEL,
            message_type=StoredMessageType.WW_MESSAGE,
            message_id=message.id,
        )

        return f"Successfully created {month} wishing well: {message.jump_url}"

    @handle_command_errors
    async def update(self) -> str:
        new_entries = await self._service.update()

        await self._update_stored_message()
        return f"Successfully updated! {len(new_entries)} new entries found."

    @handle_command_errors
    async def add_entry(self, member: str, mc_amount: int) -> str:
        await self._service.add_entry(member, mc_amount)

        await self._update_stored_message()
        return f"Successfully added {mc_amount} MCs for {member}."

    @handle_command_errors
    async def withdraw_member(self, member: str) -> str:
        mc_amount = await self._service.withdraw_member(member)

        await self._update_stored_message()
        return f"Successfully withdrew {mc_amount} MCs for {member}."

    @handle_command_errors
    async def withdraw_member_excess(self, member: str) -> str:
        mc_amount = await self._service.withdraw_member_excess(member)

        await self._update_stored_message()
        return f"Successfully withdrew excess {mc_amount} MCs for {member}."

    @handle_command_errors
    async def add_anonymous_entry(self, mc_amount: int) -> str:
        await self._service.add_anonymous_entry(mc_amount)

        await self._update_stored_message()
        return f"Successfully added {mc_amount} MCs to the anonymous pool."

    @handle_command_errors
    async def withdraw_anonymous_entry(self, mc_amount: int) -> str:
        await self._service.withdraw_anonymous_entry(mc_amount)

        await self._update_stored_message()
        return f"Successfully withdrew {mc_amount} MCs from the anonymous pool."

    @handle_command_errors
    async def get_all_entries(
        self,
    ) -> str:
        anonymous_entries, member_entries = await self._service.get_all_entries()
        res: str = self._message_formatter.format_private_list(
            anonymous_entries=anonymous_entries,
            member_entries=member_entries,
        )
        return res
