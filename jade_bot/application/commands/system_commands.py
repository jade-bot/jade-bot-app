from domain.services.system_service import SystemService
from infrastructure.utilities import handle_command_errors
from domain.models.system_dto import (
    StoredChannelType,
    StoredMessageType,
)
from infrastructure.message_formatters.system_messages_formatter import (
    SystemMessagesFormatter,
)
from loguru import logger


class SystemCommands:
    def __init__(self, service: SystemService):
        self._service = service
        self._message_formatter = SystemMessagesFormatter

    @handle_command_errors
    async def save_app_data(self) -> str:
        await self._service.save_app_data()
        return f"Successfully saved bot state"

    @handle_command_errors
    async def set_channel(self, channel_type: str, channel_id: int) -> str:
        channel = await self._service.set_channel(
            channel_type=StoredChannelType(channel_type), channel_id=channel_id
        )
        return f"{channel_type} has been set to {channel.jump_url}"

    @handle_command_errors
    async def set_message(self, message_type: str, message_id: int) -> str:
        message = await self._service.set_message(
            message_type=StoredMessageType(message_type), message_id=message_id
        )
        return f"{message_type} has been set to {message.jump_url}"

    @handle_command_errors
    async def get_config(self) -> str:
        channel_config, message_config = await self._service.get_config()
        response: str = self._message_formatter.format_config_list(
            channel_configs=channel_config, message_configs=message_config
        )
        return response

    @handle_command_errors
    async def get_last_ww_id(self) -> str:
        return await self._service.get_last_ww_id()

    @handle_command_errors
    async def get_last_donations_id(self) -> str:
        return await self._service.get_last_donations_id()
