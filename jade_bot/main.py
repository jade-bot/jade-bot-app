#!/usr/bin/env python3
import asyncio
import nest_asyncio
from config import TOKEN, GUILD_ID, DATA_PATH, LAST_LOG_ID

from application.commands.wishing_well_commands import WishingWellCommands
from application.commands.donations_commands import DonationsCommands
from application.commands.system_commands import SystemCommands
from application.commands.background_commands import BackgroundCommands

from application.handlers.wishing_well_handlers import (
    WWUpdateFetchedEventHandler,
    RepeatedEntryEventHandler,
    ExcessMCDepositedEventHandler,
    MemberLeftEventHandler,
)
from application.handlers.donation_handlers import (
    BankUpdateFetchedEventHandler,
    DonationFoundEventHandler,
    WithdrawalFoundEventHandler,
)
from application.handlers.system_handlers import (
    ChannelConfigChangedEventHandler,
    MessageConfigChangedEventHandler,
)
from application.handlers.common_handlers import (
    FaultyApiEventHandler,
)

from domain.events.event_publisher import EventsPublisher
from domain.events.domain_events.wishing_well_events import (
    WWUpdateFetchedEvent,
    RepeatedEntryEvent,
    ExcessMCDepositedEvent,
    MemberLeftEvent,
)
from domain.events.domain_events.donation_events import (
    BankUpdateFetchedEvent,
    DonationFoundEvent,
    WithdrawalFoundEvent,
)
from domain.events.domain_events.system_events import (
    ChannelConfigChangedEvent,
    MessageConfigChangedEvent,
)
from domain.events.domain_events.common_events import FaultyApiEvent

from domain.repositories.wishing_well_repository import WishingWellEntryRepository
from domain.repositories.donations_repository import DonationsRepository
from domain.repositories.system_repository import (
    ChannelConfigRepository,
    MessageConfigRepository,
)

from domain.services.wishing_well_service import WishingWellService
from domain.services.donation_service import DonationsService
from domain.services.system_service import SystemService

from domain.models.system_dto import StoredChannelType

from infrastructure.discord.notification_service import DiscordNotificationService
from infrastructure.discord.discord_service import DiscordService
from infrastructure.data_persistence import DataPersistenceManager, RepositoryTypes
from infrastructure.gw2api.api import GW2API
from domain.config_processing_service import ConfigProcessingService

from presentation.bot import get_bot
from presentation.cogs import (
    WishingWellCog,
    HelpCommandCog,
    SystemCog,
    BackgroundCog,
    DonationsCog,
)
from loguru import logger

nest_asyncio.apply()


# noinspection PyTypeChecker
def setup_ww_events_handlers(
    events_publisher: EventsPublisher,
    bank_notification_service: DiscordNotificationService,
    default_notification_service: DiscordNotificationService,
) -> EventsPublisher:
    # fetched update
    update_fetched_handler = WWUpdateFetchedEventHandler(
        notification_service=default_notification_service
    )
    events_publisher.subscribe(
        event_type=WWUpdateFetchedEvent, handler=update_fetched_handler.handle
    )

    # repeated entry
    repeated_entry_handler = RepeatedEntryEventHandler(
        notification_service=bank_notification_service
    )
    events_publisher.subscribe(
        event_type=RepeatedEntryEvent, handler=repeated_entry_handler.handle
    )

    # excess entry
    excess_entry_handler = ExcessMCDepositedEventHandler(
        notification_service=bank_notification_service
    )
    events_publisher.subscribe(
        event_type=ExcessMCDepositedEvent, handler=excess_entry_handler.handle
    )

    # member left
    member_left_handler = MemberLeftEventHandler(
        notification_service=default_notification_service
    )
    events_publisher.subscribe(
        event_type=MemberLeftEvent, handler=member_left_handler.handle
    )

    return events_publisher


# noinspection PyTypeChecker
def setup_donation_events_handlers(
    events_publisher: EventsPublisher,
    bank_notification_service: DiscordNotificationService,
    default_notification_service: DiscordNotificationService,
) -> EventsPublisher:
    # update fetched
    update_fetched_handler = BankUpdateFetchedEventHandler(
        notification_service=default_notification_service
    )
    events_publisher.subscribe(
        event_type=BankUpdateFetchedEvent, handler=update_fetched_handler.handle
    )

    # donation found
    donation_found_handler = DonationFoundEventHandler(
        notification_service=bank_notification_service
    )
    events_publisher.subscribe(
        event_type=DonationFoundEvent, handler=donation_found_handler.handle
    )

    # withdrawal found
    withdrawal_found_handler = WithdrawalFoundEventHandler(
        notification_service=bank_notification_service
    )
    events_publisher.subscribe(
        event_type=WithdrawalFoundEvent, handler=withdrawal_found_handler.handle
    )

    return events_publisher


# noinspection PyTypeChecker
def setup_system_events_handlers(
    events_publisher: EventsPublisher,
    default_notification_service: DiscordNotificationService,
    discord_service: DiscordService,
) -> EventsPublisher:
    # channel config changed
    channel_changed_handler = ChannelConfigChangedEventHandler(
        notification_service=default_notification_service,
        discord_service=discord_service,
    )

    events_publisher.subscribe(
        event_type=ChannelConfigChangedEvent, handler=channel_changed_handler.handle
    )

    # saved message config changed
    message_changed_handler = MessageConfigChangedEventHandler(
        notification_service=default_notification_service,
        discord_service=discord_service,
    )

    events_publisher.subscribe(
        event_type=MessageConfigChangedEvent, handler=message_changed_handler.handle
    )

    # faulty API encountered
    faulty_api_handler = FaultyApiEventHandler(
        notification_service=default_notification_service
    )
    events_publisher.subscribe(
        event_type=FaultyApiEvent, handler=faulty_api_handler.handle
    )

    return events_publisher


async def main() -> None:
    bot = get_bot()
    gw_api = GW2API(guild_id=GUILD_ID)
    discord_service = DiscordService(bot=bot)

    # setup repositories
    persistence_manager = DataPersistenceManager(data_path=DATA_PATH)

    persistence_manager.register_data(
        default_factory=ChannelConfigRepository,
        name=RepositoryTypes.DISCORD_CHANNELS.value,
    )

    persistence_manager.register_data(
        default_factory=MessageConfigRepository,
        name=RepositoryTypes.DISCORD_MESSAGES.value,
    )

    persistence_manager.register_data(
        default_factory=WishingWellEntryRepository,
        name=RepositoryTypes.WISHING_WELL_ENTRIES.value,
    )

    persistence_manager.register_data(
        default_factory=DonationsRepository,
        name=RepositoryTypes.DONATIONS.value,
    )

    channel_config_repo: ChannelConfigRepository = (
        persistence_manager.load_or_init_data(RepositoryTypes.DISCORD_CHANNELS.value)
    )
    message_config_repo: MessageConfigRepository = (
        persistence_manager.load_or_init_data(RepositoryTypes.DISCORD_MESSAGES.value)
    )
    wishing_well_entries_repo: WishingWellEntryRepository = (
        persistence_manager.load_or_init_data(
            RepositoryTypes.WISHING_WELL_ENTRIES.value
        )
    )
    if wishing_well_entries_repo.last_log_id == 0:
        wishing_well_entries_repo.last_log_id = LAST_LOG_ID

    donations_repo: DonationsRepository = persistence_manager.load_or_init_data(
        RepositoryTypes.DONATIONS.value
    )

    if donations_repo.last_log_id == 0:
        donations_repo.last_log_id = LAST_LOG_ID

    config_processor = ConfigProcessingService(
        message_repository=message_config_repo,
        channel_repository=channel_config_repo,
    )

    default_notification_service = DiscordNotificationService(
        discord_service=discord_service,
        channel_config=config_processor.get_channel_config(
            channel_type=StoredChannelType.BOT_LOG_CHANNEL
        ),
    )
    bank_notification_service = DiscordNotificationService(
        discord_service=discord_service,
        channel_config=config_processor.get_channel_config(
            channel_type=StoredChannelType.BANK_CHANNEL
        ),
    )

    events_publisher = EventsPublisher()

    # set up and subscribe event handlers
    events_publisher = setup_ww_events_handlers(
        events_publisher=events_publisher,
        bank_notification_service=bank_notification_service,
        default_notification_service=default_notification_service,
    )

    events_publisher = setup_donation_events_handlers(
        events_publisher=events_publisher,
        bank_notification_service=bank_notification_service,
        default_notification_service=default_notification_service,
    )

    events_publisher = setup_system_events_handlers(
        events_publisher=events_publisher,
        default_notification_service=default_notification_service,
        discord_service=discord_service,
    )

    # start domain services
    wishing_well_service = WishingWellService(
        events_publisher=events_publisher,
        repository=wishing_well_entries_repo,
        api=gw_api,
    )

    donations_service = DonationsService(
        events_publisher=events_publisher, repository=donations_repo, api=gw_api
    )

    system_service = SystemService(
        events_publisher=events_publisher,
        channel_repository=channel_config_repo,
        message_repository=message_config_repo,
        ww_entries_repository=wishing_well_entries_repo,
        donations_repository=donations_repo,
        persistence_manager=persistence_manager,
        discord_service=discord_service,
        config_processor=config_processor,
    )

    # start application services
    wishing_well_commands = WishingWellCommands(
        service=wishing_well_service,
        discord_service=discord_service,
        config_processor=config_processor,
    )
    donation_commands = DonationsCommands(
        service=donations_service,
        discord_service=discord_service,
        config_processor=config_processor,
    )

    system_commands = SystemCommands(service=system_service)

    background_commands = BackgroundCommands(
        discord_service=discord_service,
        config_processor=config_processor,
    )

    # add cogs
    await bot.add_cog(HelpCommandCog(bot=bot))
    await bot.add_cog(WishingWellCog(bot=bot, commands_service=wishing_well_commands))
    await bot.add_cog(SystemCog(bot=bot, commands_service=system_commands))
    await bot.add_cog(
        BackgroundCog(
            bot=bot,
            ww_commands_service=wishing_well_commands,
            donations_commands_service=donation_commands,
            background_commands_service=background_commands,
            system_commands_service=system_commands,
        )
    )
    await bot.add_cog(DonationsCog(bot=bot, commands_service=donation_commands))

    bot.run(TOKEN)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
