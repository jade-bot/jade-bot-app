import os


def get_env_int(var_name: str, default: int | None = None) -> int | None:
    value = os.getenv(var_name)
    if value is not None:
        try:
            return int(value)
        except ValueError:
            pass
    return default


# channels
WISHING_WELL_MESSAGE_ID = get_env_int("WISHING_WELL_MESSAGE_DEFAULT")
BOT_LOG_CHANNEL_ID = get_env_int("NOTIFICATION_CHANNEL_DEFAULT")
WISHING_WELL_CHANNEL_ID = get_env_int("WISHING_WELL_CHANNEL_DEFAULT")
BANK_CHANNEL_ID = get_env_int("BANK_CHANNEL_DEFAULT")
VOTING_CHANNEL = get_env_int("VOTING_CHANNEL_DEFAULT")
COMMANDERS_CHANNEL = get_env_int("EVENT_HOSTS_CHANNEL_DEFAULT")

# IDs and auth params
TOKEN = os.environ.get("DISCORD_TOKEN")
GUILD_ID = os.environ.get("GUILD_ID")
AUTHENTICATION_TOKEN = os.environ.get("AUTHENTICATION_TOKEN")
LAST_LOG_ID = os.environ.get("LAST_LOG_ID")

DATA_PATH = os.environ.get("DATA_PATH", "/jade_bot/data_storage")
