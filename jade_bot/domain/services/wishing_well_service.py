from typing import Any
from loguru import logger

from domain.models.wishing_well_dto import WishingWellEntry
from domain.events.event_publisher import EventsPublisher
from domain.events.domain_events.wishing_well_events import (
    RepeatedEntryEvent,
    ExcessMCDepositedEvent,
    WWUpdateFetchedEvent,
    MemberLeftEvent,
)
from domain.events.domain_events.common_events import FaultyApiEvent
from domain.repositories.wishing_well_repository import WishingWellEntryRepository
from domain.exceptions.wishing_well_exceptions import (
    MemberNotEnteredWWException,
    MemberNotInTheGuildException,
    MinimalDonationNotReachedException,
)
from infrastructure.gw2api.api import GW2API
from infrastructure.data_processing.wishing_well_processor import WishingWellProcessor
from datetime import datetime, timezone
import time


class WishingWellService:
    BASE_AMOUNT = 2

    def __init__(
        self,
        events_publisher: EventsPublisher,
        repository: WishingWellEntryRepository,
        api: GW2API,
    ):
        self.events_publisher = events_publisher
        self.repository = repository
        self.api = api
        self.data_processor = WishingWellProcessor()

    async def _remove_non_guildies(self) -> None:
        for member in self.repository.members:
            if member not in [e.name for e in await self.api.guild.get_members()]:
                entry = self.repository.get(member)
                self.repository.disown(member)
                await self.events_publisher.publish(MemberLeftEvent(entry.member))

    async def _get_mc_worth(self) -> int:
        mc_worth = await self.api.commerce.get_item_price(WishingWellProcessor.MC_ID)
        mc_price: int = mc_worth.sells.unit_price
        return mc_price

    async def clear(self) -> None:
        self.repository.clear()

    async def update(self) -> list[Any] | list[WishingWellEntry] | None:
        logger.debug(f"Requesting since {self.repository.last_log_id}")
        log = await self.api.guild.get_log(since=self.repository.last_log_id)

        self.repository.last_update_ts = int(time.time())

        if not log:
            return []

        entries: list[WishingWellEntry] | None
        last_log_id: int
        entries, last_log_id, null_entries_found = self.data_processor.process_entries(
            log
        )

        self.repository.last_log_id = last_log_id

        if null_entries_found:
            await self.events_publisher.publish(
                FaultyApiEvent(
                    fault_type="None-type entry returned from API while processing wishing well"
                )
            )

        if not entries:
            return []

        mc_worth = await self._get_mc_worth()
        for entry in entries:
            if self.repository.check_exists(entry.member):
                await self.events_publisher.publish(
                    RepeatedEntryEvent(entry, mc_worth=mc_worth)
                )
                self.repository.increase(entry.member, entry.mc_amount)
            else:
                if entry.mc_amount > self.BASE_AMOUNT:
                    await self.events_publisher.publish(
                        ExcessMCDepositedEvent(
                            entry,
                            mc_worth=mc_worth,
                            excess_amount=entry.mc_amount - self.BASE_AMOUNT,
                        )
                    )
                self.repository.put(entry.member, entry)

        await self.events_publisher.publish(WWUpdateFetchedEvent(entries))
        await self._remove_non_guildies()

        return entries

    async def add_entry(self, member: str, mc_amount: int) -> None:
        if member not in [e.name for e in await self.api.guild.get_members()]:
            raise MemberNotInTheGuildException(member=member)

        entry = WishingWellEntry(
            member,
            mc_amount,
            date=datetime.now(tz=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"),
        )

        mc_worth = await self._get_mc_worth()
        if self.repository.check_exists(member):
            await self.events_publisher.publish(
                RepeatedEntryEvent(entry, mc_worth=mc_worth)
            )
            self.repository.increase(member, mc_amount)
            return
        elif entry.mc_amount > self.BASE_AMOUNT:
            await self.events_publisher.publish(
                ExcessMCDepositedEvent(
                    entry,
                    mc_worth=mc_worth,
                    excess_amount=entry.mc_amount - self.BASE_AMOUNT,
                )
            )
        elif entry.mc_amount < self.BASE_AMOUNT:
            raise MinimalDonationNotReachedException(
                deposited=entry.mc_amount, minimal=self.BASE_AMOUNT
            )
        self.repository.put(member, entry)  # TODO unify using entries

    async def withdraw_member(self, member: str) -> int:
        entry: WishingWellEntry = self.repository.get(member)
        if not entry:
            raise MemberNotEnteredWWException(member=member)
        self.repository.delete(member)
        amount_withdrawn: int = entry.mc_amount

        return amount_withdrawn

    async def withdraw_member_excess(self, member: str) -> int:
        entry = self.repository.get(member)
        if not entry:
            raise MemberNotEnteredWWException(member=member)
        amount_withdrawn: int = entry.mc_amount - WishingWellService.BASE_AMOUNT
        entry.update_value(WishingWellService.BASE_AMOUNT)
        self.repository.put(member, entry)

        return amount_withdrawn

    async def add_anonymous_entry(self, mc_amount: int) -> None:
        self.repository.increase_anonymous(mc_amount)

    async def withdraw_anonymous_entry(self, mc_amount: int) -> None:
        self.repository.decrease_anonymous(mc_amount)

    async def get_all_entries(self) -> tuple[int, list[WishingWellEntry]]:
        anonymous = self.repository.get_anonymous()
        members = list(self.repository.get_all().values())
        return anonymous, members

    async def get_last_update(self) -> int:
        last_update: int = self.repository.last_update_ts
        return last_update
