from typing import Any

from domain.models.donations_dto import BankEntry
from domain.events.event_publisher import EventsPublisher
from domain.repositories.donations_repository import DonationsRepository
from domain.events.domain_events.donation_events import (
    BankUpdateFetchedEvent,
    DonationFoundEvent,
    WithdrawalFoundEvent,
)
from domain.events.domain_events.common_events import FaultyApiEvent
from infrastructure.gw2api.api import GW2API
from infrastructure.data_processing.donation_processor import (
    DonationsProcessor,
    EntryOperation,
    GuildLogEntryStash,
)


class DonationsService:
    MIN_VALUE: int = 10000

    def __init__(
        self,
        events_publisher: EventsPublisher,
        repository: DonationsRepository,
        api: GW2API,
    ):
        self.events_publisher = events_publisher
        self.repository = repository
        self.api = api
        self.data_processor = DonationsProcessor(api=self.api)

    async def update(self) -> list[Any] | list[BankEntry] | None:
        log = await self.api.guild.get_log(since=self.repository.last_log_id)
        if not log:
            return []

        entries: list[GuildLogEntryStash] | None
        last_log_id: int
        (
            entries,
            last_log_id,
            null_entries_found,
        ) = await self.data_processor.preprocess_log(log)

        self.repository.last_log_id = last_log_id

        if null_entries_found:
            await self.events_publisher.publish(
                FaultyApiEvent(
                    fault_type="None-type entry returned from API while processing donations"
                )
            )

        if not entries:
            return []

        donation_entries = await self.data_processor.process_entries(
            entries=entries, operation=EntryOperation.DONATION
        )
        withdrawal_entries = await self.data_processor.process_entries(
            entries=entries, operation=EntryOperation.WITHDRAWAL
        )

        total_entries_filtered = [
            e
            for e in donation_entries + withdrawal_entries
            if abs(e.total_worth) >= DonationsService.MIN_VALUE
        ]
        members = list(set([e.member for e in total_entries_filtered]))

        if total_entries_filtered:
            await self.events_publisher.publish(
                BankUpdateFetchedEvent(
                    amount=len(total_entries_filtered), members=members
                )
            )

        for entry in donation_entries:
            if entry.total_worth >= DonationsService.MIN_VALUE:
                await self.events_publisher.publish(DonationFoundEvent(entry=entry))

        for entry in withdrawal_entries:
            if abs(entry.total_worth) >= DonationsService.MIN_VALUE:
                await self.events_publisher.publish(WithdrawalFoundEvent(entry=entry))

        return entries
