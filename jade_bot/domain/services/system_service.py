from domain.events.event_publisher import EventsPublisher
from domain.events.domain_events.system_events import (
    ChannelConfigChangedEvent,
    MessageConfigChangedEvent,
)
from domain.repositories.system_repository import (
    ChannelConfigRepository,
    MessageConfigRepository,
)
from domain.models.system_dto import (
    StoredChannelType,
    StoredMessageType,
    DiscordMessageConfig,
    DiscordChannelConfig,
)
from domain.repositories.wishing_well_repository import WishingWellEntryRepository
from domain.repositories.donations_repository import DonationsRepository
from infrastructure.data_persistence import DataPersistenceManager, RepositoryTypes
from infrastructure.discord.discord_service import DiscordService
from domain.config_processing_service import (
    get_channel_type_for_message_type,
    ConfigProcessingService,
)
from typing import Any
import discord
from loguru import logger


class SystemService:
    def __init__(
        self,
        events_publisher: EventsPublisher,
        channel_repository: ChannelConfigRepository,
        message_repository: MessageConfigRepository,
        ww_entries_repository: WishingWellEntryRepository,
        donations_repository: DonationsRepository,
        persistence_manager: DataPersistenceManager,
        discord_service: DiscordService,
        config_processor: ConfigProcessingService,
    ):
        self.events_publisher = events_publisher

        self.channel_repository = channel_repository
        self.message_repository = message_repository
        self.ww_entries_repository = ww_entries_repository
        self.donations_repository = donations_repository

        self.persistence_manager = persistence_manager
        self.discord_service = discord_service
        self.config_processor = config_processor

    async def save_app_data(self) -> None:
        repositories = (
            (self.message_repository, RepositoryTypes.DISCORD_MESSAGES.value),
            (self.channel_repository, RepositoryTypes.DISCORD_CHANNELS.value),
            (self.ww_entries_repository, RepositoryTypes.WISHING_WELL_ENTRIES.value),
            (self.donations_repository, RepositoryTypes.DONATIONS.value),
        )

        for repo, name in repositories:
            self.persistence_manager.save_data(data=repo, name=name)

    async def load_app_data(self, name: RepositoryTypes) -> Any:
        return self.persistence_manager.load_or_init_data(name.value)

    async def set_channel(
        self, channel_type: StoredChannelType, channel_id: int
    ) -> discord.TextChannel:
        channel_config = DiscordChannelConfig(
            channel_type=channel_type, channel_id=channel_id
        )
        channel: discord.TextChannel = await self.discord_service.get_channel(
            channel_config
        )
        channel_config = self.config_processor.set_channel_config(
            channel_type=channel_type, channel_id=channel_id
        )
        await self.events_publisher.publish(
            ChannelConfigChangedEvent(channel_config=channel_config)
        )

        return channel

    async def set_message(
        self, message_type: StoredMessageType, message_id: int
    ) -> discord.Message:
        current_message = self.config_processor.get_message_config(
            message_type=message_type
        )

        channel_type: StoredChannelType = (
            current_message.channel_type
            if current_message
            else get_channel_type_for_message_type(message_type=message_type)
        )

        channel_config = self.config_processor.get_channel_config(
            channel_type=channel_type
        )

        message_config = DiscordMessageConfig(
            channel_type=channel_type, message_type=message_type, message_id=message_id
        )
        message: discord.Message = await self.discord_service.get_message(
            channel_config, message_config
        )

        message_config = self.config_processor.set_message_config(
            channel_type=channel_type, message_type=message_type, message_id=message_id
        )

        await self.events_publisher.publish(
            MessageConfigChangedEvent(
                message_config=message_config, channel_config=channel_config
            )
        )

        return message

    async def get_config(
        self,
    ) -> tuple[dict[StoredChannelType, str], dict[StoredMessageType, str]]:
        channel_configs = {}
        for _type in StoredChannelType:
            config = self.config_processor.get_channel_config(channel_type=_type)
            if config:
                channel = await self.discord_service.get_channel(channel_config=config)
                channel_configs[_type] = channel.jump_url
            else:
                channel_configs[_type] = "Not set"

        message_configs = {}
        for _type in StoredMessageType:
            config = self.config_processor.get_message_config(message_type=_type)
            if not config.message_id:
                message_configs[_type] = "Not set"
                continue
            channel_type = get_channel_type_for_message_type(message_type=_type)
            channel_config = self.config_processor.get_channel_config(
                channel_type=channel_type
            )
            if config:
                message = await self.discord_service.get_message(
                    channel_config=channel_config, message_config=config
                )
                message_configs[_type] = message.jump_url
            else:
                message_configs[_type] = "Not set"

        return channel_configs, message_configs

    async def get_last_ww_id(self):
        return self.ww_entries_repository.last_log_id

    async def get_last_donations_id(self):
        return self.donations_repository.last_log_id
