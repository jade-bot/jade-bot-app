from domain.models.wishing_well_dto import WishingWellEntry
from .base_repository import BaseRepository
from domain.exceptions.wishing_well_exceptions import NotEnoughMCInThePoolException


class WishingWellEntryRepository(BaseRepository[str, WishingWellEntry]):
    def __init__(self) -> None:
        super().__init__()
        self._anonymous_pool = 0
        self._last_log_id = 0
        self._last_update_ts = 0

    @property
    def last_log_id(self) -> int:
        return self._last_log_id

    @last_log_id.setter
    def last_log_id(self, new_value: int) -> None:
        self._last_log_id = new_value

    @property
    def last_update_ts(self) -> int:
        return self._last_update_ts

    @last_update_ts.setter
    def last_update_ts(self, new_value: int) -> None:
        self._last_update_ts = new_value

    @property
    def members(self) -> list[str]:
        return list(self._items.keys())

    def clear(self) -> None:
        self._items = dict()
        self._anonymous_pool = 0

    def check_exists(self, member: str) -> bool:
        return member in self.members

    def get(self, member: str) -> WishingWellEntry | None:
        return super().get(member)

    def get_all(self) -> dict[str, WishingWellEntry]:
        return super().get_all()

    def get_anonymous(self) -> int:
        return self._anonymous_pool

    def put(self, member: str, item: WishingWellEntry) -> None:
        if not isinstance(item, WishingWellEntry):
            raise ValueError("Entity must be an instance of WishingWellEntry")
        self._items[member] = item

    def increase(self, member: str, mc_amount: int) -> None:
        entry = self._items[member]
        new_value = entry.mc_amount + mc_amount
        entry.update_value(new_value)
        self.put(member, entry)

    def increase_anonymous(self, mc_amount: int) -> None:
        self._anonymous_pool += mc_amount

    def decrease(self, member: str, mc_amount: int) -> None:
        entry = self._items[member]
        if entry.mc_amount < mc_amount:
            raise NotEnoughMCInThePoolException(
                total=entry.mc_amount, requested=mc_amount
            )
        entry.update_value(entry.mc_amount - mc_amount)
        self.put(member, entry)

    def decrease_anonymous(self, mc_amount: int) -> None:
        if self._anonymous_pool < mc_amount:
            raise NotEnoughMCInThePoolException(
                total=self._anonymous_pool, requested=mc_amount
            )
        self._anonymous_pool -= mc_amount

    def delete(self, member: str) -> None:
        del self._items[member]

    def disown(self, member: str) -> None:
        entry = self._items[member]
        self._anonymous_pool += entry.mc_amount
        self.delete(member)
