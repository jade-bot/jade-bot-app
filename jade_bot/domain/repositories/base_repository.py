from typing import Generic, TypeVar
from abc import ABC, abstractmethod

TKey = TypeVar("TKey")
TValue = TypeVar("TValue")


class BaseRepository(ABC, Generic[TKey, TValue]):
    def __init__(self) -> None:
        self._items: dict[TKey, TValue] = dict()

    def get(self, id: TKey) -> TValue | None:
        return self._items.get(id, None)

    def get_all(self) -> dict[TKey, TValue]:
        return self._items

    @abstractmethod
    def put(self, id: TKey, item: TValue) -> None:
        pass

    @abstractmethod
    def delete(self, id: TKey) -> None:
        pass
