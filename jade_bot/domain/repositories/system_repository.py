from domain.models.system_dto import (
    DiscordChannelConfig,
    DiscordMessageConfig,
    StoredChannelType,
    StoredMessageType,
)
from .base_repository import BaseRepository
from config import (
    WISHING_WELL_MESSAGE_ID,
    BOT_LOG_CHANNEL_ID,
    WISHING_WELL_CHANNEL_ID,
    BANK_CHANNEL_ID,
    VOTING_CHANNEL,
    COMMANDERS_CHANNEL,
)
from loguru import logger


class ChannelConfigRepository(BaseRepository[StoredChannelType, DiscordChannelConfig]):
    def __init__(self) -> None:
        super().__init__()

        # use pre-defined values from .env
        self._items[StoredChannelType.BOT_LOG_CHANNEL] = DiscordChannelConfig(
            channel_type=StoredChannelType.BOT_LOG_CHANNEL,
            channel_id=BOT_LOG_CHANNEL_ID,
        )
        self._items[StoredChannelType.WISHING_WELL_CHANNEL] = DiscordChannelConfig(
            channel_type=StoredChannelType.WISHING_WELL_CHANNEL,
            channel_id=WISHING_WELL_CHANNEL_ID,
        )
        self._items[StoredChannelType.BANK_CHANNEL] = DiscordChannelConfig(
            channel_type=StoredChannelType.BANK_CHANNEL, channel_id=BANK_CHANNEL_ID
        )

        self._items[StoredChannelType.VOTING_CHANNEL] = DiscordChannelConfig(
            StoredChannelType.VOTING_CHANNEL, VOTING_CHANNEL
        )
        self._items[StoredChannelType.COMMANDERS_CHANNEL] = DiscordChannelConfig(
            StoredChannelType.COMMANDERS_CHANNEL, COMMANDERS_CHANNEL
        )

    def get(self, channel_type: StoredChannelType) -> DiscordChannelConfig | None:
        return super().get(channel_type)

    def get_all(self) -> dict[StoredChannelType, DiscordChannelConfig]:
        return super().get_all()

    def put(
        self, channel_type: StoredChannelType, entity: DiscordChannelConfig
    ) -> None:
        if not isinstance(entity, DiscordChannelConfig):
            raise ValueError("Entity must be an instance of DiscordChannelConfig")
        self._items[channel_type] = entity

    def delete(self, channel_type: StoredChannelType) -> None:
        if channel_type in self._items:
            del self._items[channel_type]


class MessageConfigRepository(BaseRepository[StoredMessageType, DiscordMessageConfig]):
    def __init__(self) -> None:
        super().__init__()

        # use pre-defined values from .env
        self._items[StoredMessageType.WW_MESSAGE] = DiscordMessageConfig(
            channel_type=StoredChannelType.WISHING_WELL_CHANNEL,
            message_type=StoredMessageType.WW_MESSAGE,
            message_id=WISHING_WELL_MESSAGE_ID,
        )

    def get(self, message_type: StoredMessageType) -> DiscordMessageConfig | None:
        response = super().get(message_type)
        return response

    def get_all(self) -> dict[StoredMessageType, DiscordMessageConfig]:
        return super().get_all()

    def put(
        self, message_type: StoredMessageType, entity: DiscordMessageConfig
    ) -> None:
        if not isinstance(entity, DiscordMessageConfig):
            raise ValueError("Entity must be an instance of DiscordMessageConfig")
        self._items[message_type] = entity

    def delete(self, message_type: StoredMessageType) -> None:
        if message_type in self._items:
            del self._items[message_type]
