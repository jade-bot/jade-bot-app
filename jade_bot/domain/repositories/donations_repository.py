from .base_repository import BaseRepository


class DonationsRepository(BaseRepository[str, str]):
    def __init__(self) -> None:
        super().__init__()
        self._anonymous_pool = 0
        self._last_log_id = 0
        self._last_update_ts = 0

    @property
    def last_log_id(self) -> int:
        return self._last_log_id

    @last_log_id.setter
    def last_log_id(self, new_value: int) -> None:
        self._last_log_id = new_value

    def put(self, id: str, item: str) -> None:
        pass

    def delete(self, id: str) -> None:
        pass
