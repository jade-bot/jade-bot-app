class JadeBotException(Exception):
    def __init__(self, msg: str | None = None):
        if msg:
            self.msg = msg
        else:
            self.msg = ""

    def __repr__(self) -> str:
        return self.msg

    def __str__(self) -> str:
        return self.msg
