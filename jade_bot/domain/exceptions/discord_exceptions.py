from domain.exceptions.base_exception import JadeBotException
from domain.models.system_dto import DiscordChannelConfig, DiscordMessageConfig
from discord import Message


class ChannelNotFoundException(JadeBotException):
    def __init__(self, channel: DiscordChannelConfig) -> None:
        super().__init__(
            f"Channel with id {channel.channel_id} was not found in the server or cannot be used to send messages to"
        )


class MessageNotFoundException(JadeBotException):
    def __init__(self, message: DiscordMessageConfig) -> None:
        super().__init__(
            f"Message with id `{message.message_id}` was not found in the corresponding channel ({message.channel_type.value})"
        )


class CannotCreateThreadException(JadeBotException):
    def __init__(self, channel: DiscordChannelConfig) -> None:
        super().__init__(
            f"Channel with id `{channel.channel_id}` cannot be used to create threads"
        )


class RoleNotFoundException(JadeBotException):
    def __init__(self, role: str) -> None:
        super().__init__(f"Role {role} was not found in the server")


class MessageNotAvailableException(JadeBotException):
    def __init__(self, message: Message) -> None:
        super().__init__(
            f"{message.jump_url} was not posted by the bot and cannot be used"
        )
