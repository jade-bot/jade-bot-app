from domain.exceptions.base_exception import JadeBotException


class MemberNotEnteredWWException(JadeBotException):
    def __init__(self, member: str) -> None:
        super().__init__(f"{member} was not found in the WW")


class MemberNotInTheGuildException(JadeBotException):
    def __init__(self, member: str) -> None:
        super().__init__(f"{member} was not found in the guild!")


class WWNotRunningException(JadeBotException):
    def __init__(self) -> None:
        super().__init__("No running WW found")


class NotEnoughMCInThePoolException(JadeBotException):
    def __init__(self, total: int, requested: int) -> None:
        super().__init__(
            f"Not enough MC in the pool: requested: {requested}, available: {total}"
        )


class MinimalDonationNotReachedException(JadeBotException):
    def __init__(self, deposited: int, minimal: int) -> None:
        super().__init__(
            f"Donated amount is below mininal donation: required min {minimal} MCs, deposited {deposited}"
        )
