from .domain_events.base_event import BaseEvent
from typing import Callable, Coroutine


class EventsPublisher:
    def __init__(self) -> None:
        self.handlers: dict[
            type[BaseEvent], list[Callable[[BaseEvent], Coroutine]]
        ] = dict()

    def subscribe(
        self, event_type: type[BaseEvent], handler: Callable[[BaseEvent], Coroutine]
    ) -> None:
        if event_type not in self.handlers:
            self.handlers[event_type] = []
        self.handlers[event_type].append(handler)

    async def publish(self, event: BaseEvent) -> None:
        event_type = type(event)
        if event_type in self.handlers:
            for handler in self.handlers[event_type]:
                await handler(event)
