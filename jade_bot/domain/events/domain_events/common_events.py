from .base_event import BaseEvent


class FaultyApiEvent(BaseEvent):
    def __init__(self, fault_type: str) -> None:
        self.fault_type = fault_type
