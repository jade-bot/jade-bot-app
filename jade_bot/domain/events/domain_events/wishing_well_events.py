from .base_event import BaseEvent
from domain.models.wishing_well_dto import WishingWellEntry


class WWUpdateFetchedEvent(BaseEvent):
    def __init__(self, entries: list[WishingWellEntry]) -> None:
        self.entries = entries


class RepeatedEntryEvent(BaseEvent):
    def __init__(self, entry: WishingWellEntry, mc_worth: int) -> None:
        self.entry = entry
        self.mc_worth = mc_worth


class ExcessMCDepositedEvent(BaseEvent):
    def __init__(
        self, entry: WishingWellEntry, mc_worth: int, excess_amount: int
    ) -> None:
        self.entry = entry
        self.mc_worth = mc_worth
        self.excess_amount = excess_amount


class MemberLeftEvent(BaseEvent):
    def __init__(self, member: str) -> None:
        self.member = member
