from .base_event import BaseEvent
from domain.models.donations_dto import BankEntry


class BankUpdateFetchedEvent(BaseEvent):
    def __init__(self, amount: int, members: list[str]) -> None:
        self.amount = amount
        self.members = members


class DonationFoundEvent(BaseEvent):
    def __init__(self, entry: BankEntry) -> None:
        self.entry = entry


class WithdrawalFoundEvent(BaseEvent):
    def __init__(self, entry: BankEntry) -> None:
        self.entry = entry
