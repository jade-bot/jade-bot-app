from .base_event import BaseEvent
from domain.models.system_dto import DiscordChannelConfig, DiscordMessageConfig


class ChannelConfigChangedEvent(BaseEvent):
    def __init__(self, channel_config: DiscordChannelConfig) -> None:
        self.channel_config = channel_config


class MessageConfigChangedEvent(BaseEvent):
    def __init__(
        self, message_config: DiscordMessageConfig, channel_config: DiscordChannelConfig
    ) -> None:
        self.message_config = message_config
        self.channel_config = channel_config
