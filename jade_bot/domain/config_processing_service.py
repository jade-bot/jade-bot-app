from domain.models.system_dto import (
    StoredMessageType,
    StoredChannelType,
    DiscordMessageConfig,
    DiscordChannelConfig,
)
from domain.repositories.system_repository import (
    MessageConfigRepository,
    ChannelConfigRepository,
)


def get_channel_type_for_message_type(
    message_type: StoredMessageType,
) -> StoredChannelType | None:
    if message_type == StoredMessageType.WW_MESSAGE:
        return StoredChannelType.WISHING_WELL_CHANNEL
    else:
        return None


class ConfigProcessingService:
    def __init__(
        self,
        message_repository: MessageConfigRepository,
        channel_repository: ChannelConfigRepository,
    ):
        self.message_repository = message_repository
        self.channel_repository = channel_repository

    def get_message_config(
        self, message_type: StoredMessageType
    ) -> DiscordMessageConfig:
        return self.message_repository.get(message_type=message_type)

    def get_channel_config(
        self, channel_type: StoredChannelType
    ) -> DiscordChannelConfig:
        return self.channel_repository.get(channel_type=channel_type)

    def set_message_config(
        self,
        channel_type: StoredChannelType,
        message_type: StoredMessageType,
        message_id: int,
    ) -> DiscordMessageConfig:
        new_config = DiscordMessageConfig(
            channel_type=channel_type, message_type=message_type, message_id=message_id
        )
        self.message_repository.put(message_type=message_type, entity=new_config)
        return new_config

    def set_channel_config(
        self, channel_type: StoredChannelType, channel_id: int
    ) -> DiscordChannelConfig:
        new_config = DiscordChannelConfig(
            channel_type=channel_type, channel_id=channel_id
        )
        self.channel_repository.put(channel_type=channel_type, entity=new_config)
        return new_config
