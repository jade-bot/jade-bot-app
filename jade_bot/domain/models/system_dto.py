from enum import Enum, auto


class StoredChannelType(Enum):
    WISHING_WELL_CHANNEL = "Wishing Well channel"
    BANK_CHANNEL = "Bank channel"
    COMMANDERS_CHANNEL = "Commanders channel"
    VOTING_CHANNEL = "Voting channel"
    BOT_LOG_CHANNEL = "Bot log channel"


class StoredMessageType(Enum):
    WW_MESSAGE = "Wishing Well public message"


class DiscordChannelConfig:
    def __init__(self, channel_type: StoredChannelType, channel_id: int):
        self._channel_type = channel_type
        self._channel_id = channel_id

    @property
    def channel_type(self) -> StoredChannelType:
        return self._channel_type

    @property
    def channel_id(self) -> int:
        return self._channel_id

    def __str__(self) -> str:
        return f"DiscordChannelConfig: channel {self._channel_type.value} - id {self._channel_id}"


class DiscordMessageConfig:
    def __init__(
        self,
        channel_type: StoredChannelType,
        message_type: StoredMessageType,
        message_id: int,
    ):
        self._channel_type = channel_type
        self._message_type = message_type
        self._message_id = message_id

    @property
    def channel_type(self) -> StoredChannelType:
        return self._channel_type

    @property
    def message_type(self) -> StoredMessageType:
        return self._message_type

    @property
    def message_id(self) -> int:
        return self._message_id

    def __str__(self) -> str:
        return f"DiscordMessageConfig: channel {self._channel_type.value} - message {self._message_type.value} - id {self._message_id}"
