class WishingWellEntry:
    def __init__(
        self, member: str, mc_amount: int, date: str, entry_log_id: int | None = None
    ):
        if mc_amount <= 0:
            raise ValueError("MC amount must be positive")
        self.member = member  # may be repetitive since member is used as a key to store
        self.mc_amount = mc_amount
        self.date = date
        self.entry_log_id = entry_log_id

    def update_value(self, mc_amount: int) -> None:
        if mc_amount < 0:
            raise ValueError("MC amount must be positive")
        self.mc_amount = mc_amount

    def format(self, show_amount: bool = True) -> str:
        return (
            f"{self.member} — {self.mc_amount} MCs" if show_amount else f"{self.member}"
        )
