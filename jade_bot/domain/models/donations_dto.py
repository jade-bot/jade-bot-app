from infrastructure.utilities import format_gold


class ItemDonation:
    def __init__(self, item_id: int, name: str, price: int, amount: int):
        self.item_id = item_id
        self.name = name
        self.price = price
        self.amount = amount

    @property
    def total_worth(self) -> int:
        return self.price * self.amount

    def format_total_price(self) -> str:
        total_price = self.price * self.amount
        formatted: str = format_gold(total_price)
        return formatted

    def format(self) -> str:
        return f"{self.name} x{self.amount} {self.format_total_price()}"


class GoldDonation:
    def __init__(self, gold: int):
        self.value = gold

    def format(self) -> str:
        formatted: str = format_gold(self.value)
        return formatted


class BankEntry:
    def __init__(
        self,
        member: str,
        gold: GoldDonation | None = None,
        items: list[ItemDonation] | None = None,
    ):
        self.member = member
        self.gold = gold if gold else GoldDonation(0)
        self.items = items if items else []
        self.items.sort(key=lambda x: x.total_worth, reverse=True)

    @property
    def total_worth(self) -> int:
        return self.gold.value + sum([i.total_worth for i in self.items])

    @property
    def item_donated_count(self) -> int:
        return len(self.items)

    def format_shorten_item_donations(self) -> str:
        limit = 7
        cut_amount = (
            self.item_donated_count - limit if self.item_donated_count > limit else 0
        )
        message = "* " + "\n* ".join([i.format() for i in self.items[:limit]])
        if cut_amount:
            message += f"\n... and {cut_amount} more"
        return message
