FROM python:3.10.14-slim

USER root

ADD ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir jade_bot
ADD ./jade_bot /jade_bot
RUN mkdir /jade_bot/data_storage

WORKDIR /jade_bot
ENV PYTHONPATH='/jade_bot'

CMD ["python", "-m", "main"]