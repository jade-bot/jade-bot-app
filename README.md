# TODO list
## Functional
* WW:
  * ~~add public list~~
  * ~~add price calculation for extra/repeated donations~~
  * ~~add public message update after every command that changes members list~~
* Donations
  * ~~add the structure~~
  * ~~price calculation~~
  * ~~shortening of long donation lists~~
  * manual entry for gold donated
* Raid roles
  * add the structure
* ~~Scheduled messages~~
  * ~~prepare messenger~~
  * ~~add to background tasks~~
* Strikes
  * TODO

## Non-functional
* ~~Add message formatters to commands classes~~
* setup config management (make config processing service a layer to the repositories?)
* move system repositories to infrastructure?
* ~~setup dump/load of in-memory repos~~
* ~~DiscordMessageService -> DiscordService, message_service -> discord_service~~
* Maybe merge discord notification service into the discord service?

# Local setup
Make sure you have .env file created with IDs of the pre-configured channels
```
docker build --tag registry.gitlab.com/jade-bot/jade-bot-app:2.0.0 .
docker-compose up
```